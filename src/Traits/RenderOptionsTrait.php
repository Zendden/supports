<?php

namespace App\Traits;

trait RenderOptionsTrait {
    /**
     * List of render parametrs
     * @var array $renderOptions
     */
    protected $renderOptions = [];

    /**
     * Add new option for transfer into Twig
     * 
     * @return self
     */
    public function addOption (string $option, $entity): self {
        $this->renderOptions["$option"] = $entity;
        return $this;
    }

    public function getOptions (): array {
        return $this->renderOptions;
    }
}