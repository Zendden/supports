<?php

namespace App\Form;

use App\Entity\ProblemTicketStore\ProblemTicketStore;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProblemTicketStoreType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("Game", EntityType::class, [
                'class' => \App\Entity\ProblemTicket\Game::class,
                'choice_label' => 'title',
                "label" => "Choose the Game:",
                // "data" => $game,
            ])
            ->add("Device", TextType::class, [
                "label" => "Device:",
            ])
            ->add("Store", EntityType::class, [
                "class" => \App\Entity\ProblemTicketStore\Store::class,
                "choice_label" => "title",
                "label" => "Store:",
            ])
            ->add("Tag", EntityType::class, [
                "class" => \App\Entity\ProblemTicketStore\TagStore::class,
                "choice_label" => "description",
                "label" => "Tag:",
            ])
            ->add("Os", TextType::class, [
                "label" => "OS:",
            ])
            ->add("Ram", NumberType::class, [
                "label" => "RAM:",
            ])
        ;

        $builder->get("Game")->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();

                $form->getParent()->add("Version", EntityType::class, [
                    "class" => \App\Entity\ProblemTicket\GameVersion::class,
                    "choices" => $form->getData()->getVersions(),
                    "choice_label" => "title",
                    "label" => "Version:",
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => ProblemTicketStore::class,
        ));
    }
}
