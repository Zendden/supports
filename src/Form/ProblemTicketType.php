<?php

namespace App\Form;

use App\Entity\ProblemTicket\Marker;
use App\Entity\ProblemTicket\ProblemTicket;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProblemTicketType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("Game", EntityType::class, [
                'class' => \App\Entity\ProblemTicket\Game::class,
                'choice_label' => 'title',
                "label" => "Choose the Game:",
            ])
            ->add("Server", EntityType::class, [
                "class" => \App\Entity\ProblemTicket\Server::class,
                "choice_label" => "title",
                "label" => "Server:",
            ])
            ->add("PlayerID", NumberType::class, [
                "label" => "ID: ",
            ])
            ->add("Platform", EntityType::class, [
                "class" => \App\Entity\ProblemTicket\Platform::class,
                "choice_label" => "title",
                "label" => "Platform:",
            ])
            ->add("Tag", EntityType::class, [
                "class" => \App\Entity\ProblemTicket\Tag::class,
                "choice_label" => "description",
                "label" => "Tag:",
            ])
            ->add("Note", TextareaType::class, [
                "label" => "Note by ticket:",
            ])
            ->add("Link", TextareaType::class, [
                "label" => "Link:",
            ])
            ->add("Created", DateTimeType::class, [
                'label' => 'Created at: ',
                'required' => true,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control input-inline datetimepicker',
                    'data-provide' => 'datetimepicker',
                    'html5' => false,
                ]]);

        $builder->get("Tag")->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();

                $form->getParent()->add("marker", EntityType::class, [
                    "class" => \App\Entity\ProblemTicket\Marker::class,
                    "choice_label" => "description",
                    "choices" => $form->getData()->getMarker(),
                    "label" => "Marker:",
                ]);
            }
        );

        $builder->get("Game")->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {

                $form = $event->getForm();

                $form->getParent()->add("Version", EntityType::class, [
                    "class" => \App\Entity\ProblemTicket\GameVersion::class,
                    "choices" => $form->getData()->getVersions(),
                    "choice_label" => "title",
                    "label" => "Version:",
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => ProblemTicket::class,
        ));
    }
}
