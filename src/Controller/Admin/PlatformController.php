<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\Platform;
use App\Model\ProblemTicket\PlatformModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlatformController extends AbstractController
{

    /**
     * Add the new Platform
     *
     * @Route("/add/platform/", name="add-platform")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $platform = new Platform();

        $form = $this->createFormBuilder($platform)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Platform", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $platform = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($platform);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-platforms-page"
            );
        }

        return $this->render(
            "./Admin/Platform/platform.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/platform/", name="edit-platform")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Platform $platformEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $platform = $entityManager->getRepository(Platform::class)->find($id);

        $form = $this->createFormBuilder($platform)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($platform);
            $entityManager->flush();

            return $this->redirectToRoute("show-platforms-page");
        }

        return $this->render(
            "./Sdmin/Platform/platform.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/platform", name="delete-platform")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $platform = $entityManager->getRepository(Platform::class)->find($id);

        $entityManager->remove($platform);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-platforms-page"
        );
    }

    /**
     * @Route("show/{page}/page/platform", name="show-platforms-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, PlatformModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Platform/platformList.html.twig", [
                "platforms" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/platform", name="show-one-platform")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $platform = $entityManager->getRepository(Platform::class)->find($id);

        return $this->render(
            "/Admin/Platform/platformCRUD.html.twig", [
                "platform" => $platform,
                "buttons" => "",
            ]
        );
    }
}
