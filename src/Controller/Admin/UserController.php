<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Model\UserModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    /**
     * Add the new User
     *
     * @Route("/add/user/", name="add-user")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New User", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-users-page"
            );
        }

        return $this->render(
            "./Admin/User/user.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/user/", name="edit-user")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, User $userEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        $form = $this->createFormBuilder($user)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute("show-users-page");
        }

        return $this->render(
            "./Admin/User/user.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/user", name="delete-user")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $user = $entityManager->getRepository(User::class)->find($id);

        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-users-page"
        );
    }

    /**
     * @Route("show/{page}/page/user", name="show-users-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, UserModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/User/userList.html.twig", [
                "users" => $puginationRows,
                "buttons" => $puginationButtons,
            ]
        );
    }

    /**
     * @Route("show/{id}/user", name="show-one-user")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find($id);

        return $this->render(
            "/Admin/User/userCRUD.html.twig", [
                "user" => $user,
                "buttons" => "",
            ]
        );
    }
}
