<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicketStore\Store;
use App\Model\ProblemTicketStore\StoreModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StoreController extends AbstractController
{

    /**
     * Add the new Tag
     *
     * @Route("/add/store/", name="add-store")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $store = new Store();

        $form = $this->createFormBuilder($store)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Store", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $store = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($store);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-stores-page"
            );
        }

        return $this->render(
            "./Admin/Store/store.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/store/", name="edit-store")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Store $storeEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $store = $entityManager->getRepository(Store::class)->find($id);

        $form = $this->createFormBuilder($store)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($store);
            $entityManager->flush();

            return $this->redirectToRoute("show-stores-page");
        }

        return $this->render(
            "./Admin/Store/store.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/store", name="delete-store")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $store = $entityManager->getRepository(Store::class)->find($id);

        $entityManager->remove($store);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-stores-page"
        );
    }

    /**
     * @Route("show/{page}/page/store", name="show-stores-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, StoreModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Store/storeList.html.twig", [
                "stores" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/store", name="show-one-store")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $store = $entityManager->getRepository(Store::class)->find($id);

        return $this->render(
            "/Admin/TagStore/tag_storeCRUD.html.twig", [
                "store" => $store,
                "buttons" => "",
            ]
        );
    }
}
