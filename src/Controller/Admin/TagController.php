<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\Tag;
use App\Model\ProblemTicket\TagModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TagController extends AbstractController
{

    /**
     * Add the new Tag
     *
     * @Route("/add/tag/", name="add-tag")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $tag = new Tag();

        $form = $this->createFormBuilder($tag)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("description", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Tag", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-tags-page"
            );
        }

        return $this->render(
            "./Admin/Tag/tag.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/tag/", name="edit-tag")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Tag $tagEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $tag = $entityManager->getRepository(Tag::class)->find($id);

        $form = $this->createFormBuilder($tag)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("description", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($tag);
            $entityManager->flush();

            return $this->redirectToRoute("show-tags-page");
        }

        return $this->render(
            "./Admin/Tag/tag.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/tag", name="delete-tag")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $tag = $entityManager->getRepository(Tag::class)->find($id);

        $entityManager->remove($tag);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-tags-page"
        );
    }

    /**
     * @Route("show/{page}/page/tag", name="show-tags-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, TagModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Tag/tagList.html.twig", [
                "tags" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/tag", name="show-one-tag")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $tag = $entityManager->getRepository(Tag::class)->find($id);

        return $this->render(
            "/Admin/Tag/tagCRUD.html.twig", [
                "tag" => $tag,
                "buttons" => "",
            ]
        );
    }
}
