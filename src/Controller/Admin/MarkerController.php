<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\Marker;
use App\Entity\ProblemTicket\Tag;
use App\Model\ProblemTicket\MarkerModel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MarkerController extends AbstractController
{

    /**
     * Add the new Game
     *
     * @Route("/add/marker/", name="add-marker")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $form = $this->createFormBuilder(new Marker())
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("description", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("tag", EntityType::class, [
                'class' => \App\Entity\ProblemTicket\Tag::class,
                'choice_label' => 'title'])
            ->add("Save", SubmitType::class, ["label" => "Add New Marker", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-markers-page"
            );
        }

        return $this->render(
            "./Admin/Marker/marker.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/marker/", name="edit-marker")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Marker $markerEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $marker = $entityManager->getRepository(Marker::class)->find($id);

        $form = $this->createFormBuilder($marker)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("description", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($marker);
            $entityManager->flush();

            return $this->redirectToRoute("show-markers-page");
        }

        return $this->render(
            "./Admin/Marker/marker.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/marker", name="delete-marker")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $marker = $entityManager->getRepository(Marker::class)->find($id);

        $entityManager->remove($marker);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-markers-page"
        );
    }

    /**
     * @Route("show/{page}/page/marker", name="show-markers-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, MarkerModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Marker/markerList.html.twig", [
                "markers" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/marker", name="show-one-marker")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $marker = $entityManager->getRepository(Marker::class)->find($id);

        return $this->render(
            "/Admin/Marker/markerCRUD.html.twig", [
                "marker" => $marker,
                "buttons" => "",
            ]
        );
    }
}
