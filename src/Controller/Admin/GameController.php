<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\Game;
use App\Model\ProblemTicket\GameModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{

    /**
     * Add the new Game
     *
     * @Route("/add/game/", name="add-game")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $game = new Game();

        $form = $this->createFormBuilder($game)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Game", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $game = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-games-page"
            );
        }

        return $this->render(
            "./Admin/Game/game.html.twig",
            [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/game/", name="edit-game")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Game $gameEntity)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $game = $entityManager->getRepository(Game::class)->find($id);

        $form = $this->createFormBuilder($game)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute("show-games-page");
        }

        return $this->render(
            "./Admin/Game/game.html.twig",
            [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/game", name="delete-game")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $game = $entityManager->getRepository(Game::class)->find($id);

        $entityManager->remove($game);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-games-page"
        );
    }

    /**
     * @Route("show/{page}/page/game", name="show-games-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, GameModel $model)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "./Admin/Game/gameList.html.twig",
            [
                "games" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/game", name="show-one-game")
     * @param int $id
     */
    public function showOne($id = 0)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $game = $entityManager->getRepository(Game::class)->find($id);

        return $this->render(
            "./Admin/Game/gameCRUD.html.twig",
            [
                "game" => $game,
                "buttons" => "",
            ]
        );
    }
}
