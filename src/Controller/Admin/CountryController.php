<?php

namespace App\Controller\Admin;

use App\Entity\Moderator\Country;
use App\Model\Moderator\CountryModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CountryController extends AbstractController
{

    /**
     * Add the new Country
     *
     * @Route("/add/country/", name="add-country")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $country = new Country();

        $form = $this->createFormBuilder($country)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Country", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $country = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($country);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-countries-page"
            );
        }

        return $this->render(
            "./Admin/Country/country.html.twig",
            [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/country/", name="edit-country")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Country $entity)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $country = $entityManager->getRepository(Country::class)->find($id);

        $form = $this->createFormBuilder($country)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($country);
            $entityManager->flush();

            return $this->redirectToRoute("show-countries-page");
        }

        return $this->render(
            "./Admin/Country/country.html.twig",
            [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/country", name="delete-country")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $country = $entityManager->getRepository(Country::class)->find($id);

        $entityManager->remove($country);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-countries-page"
        );
    }

    /**
     * @Route("show/{page}/page/countries", name="show-countries-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, CountryModel $model)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "./Admin/Country/countryList.html.twig",
            [
                "countries" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/country", name="show-one-country")
     * @param int $id
     */
    public function showOne($id = 0)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $country = $entityManager->getRepository(Country::class)->find($id);

        return $this->render(
            "./Admin/Country/countryCRUD.html.twig",
            [
                "country" => $country,
                "buttons" => "",
            ]
        );
    }
}
