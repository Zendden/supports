<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\Server;
use App\Model\ProblemTicket\ServerModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ServerController extends AbstractController
{

    /**
     * Add the new Server
     *
     * @Route("/add/server/", name="add-server")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $server = new Server();

        $form = $this->createFormBuilder($server)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Save", SubmitType::class, ["label" => "Add New Server", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $server = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($server);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-servers-page"
            );
        }

        return $this->render(
            "./Admin/Server/server.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/server/", name="edit-server")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, Server $serverEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $server = $entityManager->getRepository(Server::class)->find($id);

        $form = $this->createFormBuilder($server)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($server);
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-servers-page"
            );
        }

        return $this->render(
            "./Admin/Server/server.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/server", name="delete-server")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $server = $entityManager->getRepository(Server::class)->find($id);

        $entityManager->remove($server);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-servers-page"
        );
    }

    /**
     * @Route("show/{page}/page/server", name="show-servers-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, ServerModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Server/serverList.html.twig", [
                "servers" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/server", name="show-one-server")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $server = $entityManager->getRepository(Server::class)->find($id);

        return $this->render(
            "/Admin/Server/serverCRUD.html.twig", [
                "server" => $server,
                "buttons" => "",
            ]
        );
    }
}
