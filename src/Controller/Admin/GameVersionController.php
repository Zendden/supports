<?php

namespace App\Controller\Admin;

use App\Entity\ProblemTicket\GameVersion;
use App\Model\ProblemTicket\VersionModel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class GameVersionController extends AbstractController
{

    /**
     * Add the new Game
     *
     * @Route("/add/version/", name="add-version")
     * @param Request $request
     * @return void
     */
    public function create(Request $request)
    {

        $form = $this->createFormBuilder(new GameVersion())
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Game", EntityType::class, [
                'class' => \App\Entity\ProblemTicket\Game::class,
                'choice_label' => 'title'])
            ->add("Save", SubmitType::class, ["label" => "Add New Version", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute(
                "show-versions-page"
            );
        }

        return $this->render(
            "./Admin/Version/version.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("edit/{id}/version/", name="edit-version")
     * @param int $id
     * @return void
     */
    public function edit($id, Request $request, GameVersion $gameVersionEntity)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $version = $entityManager->getRepository(GameVersion::class)->find($id);

        $form = $this->createFormBuilder($version)
            ->add("title", TextType::class, ["attr" => ["class" => "form-control"]])
            ->add("Edit", SubmitType::class, ["label" => "Apply changes", "attr" => ["class" => "btn btn-dark", "style" => "margin: 15px"]])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($version);
            $entityManager->flush();

            return $this->redirectToRoute("show-versions-page");
        }

        return $this->render(
            "./Admin/Version/version.html.twig", [
                "form" => $form->createView(),
            ]
        );
    }

    /**
     * @Route("delete/{id}/version", name="delete-version")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $version = $entityManager->getRepository(GameVersion::class)->find($id);

        $entityManager->remove($version);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-versions-page"
        );
    }

    /**
     * @Route("show/{page}/page/version", name="show-versions-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, VersionModel $model)
    {

        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);

        return $this->render(
            "/Admin/Version/versionList.html.twig", [
                "versions" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page
            ]
        );
    }

    /**
     * @Route("show/{id}/version", name="show-one-version")
     * @param int $id
     */
    public function showOne($id = 0)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $version = $entityManager->getRepository(GameVersion::class)->find($id);

        return $this->render(
            "/Admin/Version/versionCRUD.html.twig", [
                "version" => $version,
                "buttons" => "",
            ]
        );
    }
}
