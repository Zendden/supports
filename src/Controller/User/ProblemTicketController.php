<?php

namespace App\Controller\User;

use App\Entity\ProblemTicket\Game;
use App\Entity\ProblemTicket\GameVersion;
use App\Entity\ProblemTicket\Marker;
use App\Entity\ProblemTicket\Platform;
use App\Entity\ProblemTicket\ProblemTicket;
use App\Entity\ProblemTicket\Server;
use App\Entity\ProblemTicket\Tag;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\MarkerModel;
use App\Model\ProblemTicket\PlatformModel;
use App\Model\ProblemTicket\ProblemTicketModel;
use App\Model\ProblemTicket\ServerModel;
use App\Model\ProblemTicket\TagModel;
use App\Model\ProblemTicket\VersionModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProblemTicketController extends AbstractController
{
    /**
     * @Route("action/problem-ticket/{id}", name="action-problem-ticket")
     *
     * @param int|null $id
     * @param Request $request
     * @return void
     */
    public function index($id = null, GameModel $gamesModel, VersionModel $versionsModel, ServerModel $serversModel, PlatformModel $platformsModel, TagModel $tagsModel, MarkerModel $markersModel)
    {
        if (null !== $id) {
            $entityManager = $this->getDoctrine()->getManager();
            $problemTicket = $entityManager->getRepository(ProblemTicket::class)->find($id);

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $problemTicket = $serializer->serialize($problemTicket, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);
        }

        return $this->render(
            "./User/ProblemTicket/problem_ticket.html.twig", [
                "games" => $gamesModel->getGames(),
                "versions" => $versionsModel->getVersions(),
                "servers" => $serversModel->getServers(),
                "platforms" => $platformsModel->getPlatforms(),
                "tags" => $tagsModel->getTags(),
                "markers" => $markersModel->getMarkers(),
                "ticket" => isset ($problemTicket) ? $problemTicket : null
            ]
        );
    }

    /**
     * @Route("create/problem-ticket/{id}", name="create-problem-ticket")
     *
     * @ParamConverter("game", options={"mapping": {"game" : "id"}})
     * @ParamConverter("server", options={"mapping": {"server" : "id"}})
     * @ParamConverter("platform", options={"mapping": {"platform" : "id"}})
     * @ParamConverter("version", options={"mapping": {"version" : "id"}})
     * @ParamConverter("tag", options={"mapping": {"tag" : "id"}})
     * @ParamConverter("marker", options={"mapping": {"marker" : "id"}})
     */
    public function createTicket($id = null, Request $request)
    {

        if (   !$request->request->has("game")
            or !$request->request->has("server")
            or !$request->request->has("version")
            or !$request->request->has("tag")
            or !$request->request->has("marker")
            or !$request->request->has("platform")
            or !$request->request->has("created")) {
            exit("Fileds not fillen");
        } else {

            $entityManager = $this->getDoctrine()->getManager();

            if ($id !== null && $problemTicket = $entityManager->getRepository(ProblemTicket::class)->find($id)) {
            } else {
                $problemTicket = new ProblemTicket();
            }

            $game = $request->request->has("game")
            ? $entityManager->getRepository(Game::class)->find((int) $request->request->get("game"))
            : new Game();

            $server = $request->request->has("server")
            ? $entityManager->getRepository(Server::class)->find((int) $request->request->get("server"))
            : new Server();

            $version = $request->request->has("version")
            ? $entityManager->getRepository(GameVersion::class)->find((int) $request->request->get("version"))
            : new GameVersion();

            $platform = $request->request->has("platform")
            ? $entityManager->getRepository(Platform::class)->find((int) $request->request->get("platform"))
            : new Platform();

            $tag = $request->request->has("tag")
            ? $entityManager->getRepository(Tag::class)->find((int) $request->request->get("tag"))
            : new Tag();

            $marker = $entityManager->getRepository(Marker::class)->find((int) $request->request->get("marker"));

            $date = \DateTime::createFromFormat('d/m/Y', $request->request->get("created"));
            $createdAt = new \Datetime($date);

            $problemTicket->setGame($game);
            $problemTicket->setVersion($version);
            $problemTicket->setServer($server);
            $problemTicket->setPlayerId($request->request->get("player_id") ? $request->request->get("player_id") : 0);
            $problemTicket->setPlatform($platform);
            $problemTicket->setTag($tag);
            $problemTicket->setMarker($marker);
            $problemTicket->setNote($request->request->get("note") ? $request->request->get("note") : "");
            $problemTicket->setLink($request->request->get("link") ? $request->request->get("link") : "");
            $problemTicket->setCreated(new \DateTime($request->request->get("created")));

            $entityManager->persist($problemTicket);
            $entityManager->flush();

            return new JsonResponse(
                $problemTicket
            );
        }
    }

    /**
     * @Route("delete/{id}/problem-ticket", name="delete-problem-ticket")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $problem_ticket = $entityManager->getRepository(ProblemTicket::class)->find($id);

        $entityManager->remove($problem_ticket);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-problem-tickets-page"
        );
    }

    /**
     * @Route("show/{page}/page/problem-ticket", name="show-problem-tickets-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, ProblemTicketModel $model)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model, $page);

        return $this->render(
            "./User/ProblemTicket/problem_ticketList.html.twig",
            [
                "problem_tickets" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page,
            ]
        );
    }

    /**
     * @Route("show/{id}/problem-ticket", name="show-one-problem-ticket", methods={"post", "get"})
     * @param int $id
     */
    public function showOne($id = 1, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($request->request->has ('id')) {
            $ticketAjax = $entityManager->getRepository (ProblemTicket::class)->find ($request->request->get ('id'));

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $ticketAjax = $serializer->serialize($ticketAjax, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return new Jsonresponse (
                $ticketAjax
            );

        } else {
            $ticketTwig = $entityManager->getRepository (ProblemTicket::class)->find ($id);

            return $this->render(
                "./User/ProblemTicket/problem_ticketCRUD.html.twig",
                [
                    "problem_ticket" => $ticketTwig,
                    "buttons" => "",
                ]
            );
        }
    }
}
