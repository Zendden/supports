<?php

namespace App\Controller\User;

use App\Entity\ProblemTicketStore\ProblemTicketStore;
use App\Entity\ProblemTicketStore\Store;
use App\Entity\ProblemTicket\Game;
use App\Entity\ProblemTicket\GameVersion;
use App\Entity\ProblemTicket\Marker;
use App\Entity\ProblemTicket\Tag;
use App\Model\ProblemTicketStore\ProblemTicketStoreModel;
use App\Model\ProblemTicketStore\StoreModel;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\MarkerModel;
use App\Model\ProblemTicket\PlatformModel;
use App\Model\ProblemTicket\TagModel;
use App\Model\ProblemTicket\VersionModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProblemTicketStoreController extends AbstractController
{
    /**
     * @Route("action/problem-ticket-store/{id}", name="action-problem-ticket-store")
     *
     * @param int|null $id
     * @param Request $request
     * @return void
     */
    public function index($id = null, Request $request, GameModel $gamesModel, VersionModel $versionsModel, StoreModel $storesModel, PlatformModel $platformsModel, TagModel $tagsModel, MarkerModel $markersModel)
    {

        $entityManager = $this->getDoctrine()->getManager();

        if ($id !== null) {
            $problemTicketStore = $entityManager->getRepository(ProblemTicketStore::class)->find($id);

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $problemTicketStore = $serializer->serialize($problemTicketStore, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return $this->render(
                "./User/ProblemTicketStore/problem_ticket_store.html.twig", [
                    "games" => $gamesModel->getGames(),
                    "versions" => $versionsModel->getVersions(),
                    "stores" => $storesModel->getStores(),
                    "tags" => $tagsModel->getTags(),
                    "markers" => $markersModel->getMarkers(),
                    "ticket" => $problemTicketStore,
                ]
            );
        } else {
            return $this->render(
                "./User/ProblemTicketStore/problem_ticket_store.html.twig", [
                    "games" => $gamesModel->getGames(),
                    "versions" => $versionsModel->getVersions(),
                    "stores" => $storesModel->getStores(),
                    "tags" => $tagsModel->getTags(),
                    "markers" => $markersModel->getMarkers(),
                ]
            );
        }
    }

    /**
     * @Route("create/problem-ticket-store/{id}", name="create-problem-ticket-store")
     *
     * @ParamConverter("game", options={"mapping": {"game" : "id"}})
     * @ParamConverter("store", options={"mapping": {"store" : "id"}})
     * @ParamConverter("version", options={"mapping": {"version" : "id"}})
     * @ParamConverter("tag", options={"mapping": {"tag" : "id"}})
     * @ParamConverter("marker", options={"mapping": {"marker" : "id"}})
     */
    public function createTicket($id = null, Request $request)
    {

        if (!$request->request->has("game")
            or !$request->request->has("version")
            or !$request->request->has("store")
            or !$request->request->has("tag")
            or !$request->request->has("marker")
            or !$request->request->has("created")) {
            exit("Fileds not fillen");
        } else {

            $entityManager = $this->getDoctrine()->getManager();

            if ($id !== null && $problemTicketStore = $entityManager->getRepository(ProblemTicketStore::class)->find($id)) {
            } else {
                $problemTicketStore = new ProblemTicketStore();
            }

            $game = $request->request->has("game")
            ? $entityManager->getRepository(Game::class)->find((int) $request->request->get("game"))
            : new Game();

            $store = $request->request->has("store")
            ? $entityManager->getRepository(Store::class)->find((int) $request->request->get("store"))
            : new Store();

            $version = $request->request->has("version")
            ? $entityManager->getRepository(GameVersion::class)->find((int) $request->request->get("version"))
            : new GameVersion();

            $tag = $request->request->has("tag")
            ? $entityManager->getRepository(Tag::class)->find((int) $request->request->get("tag"))
            : new Tag();

            $marker = $request->request->has("marker")
            ? $entityManager->getRepository(Marker::class)->find((int) $request->request->get("marker"))
            : new Marker();

            $problemTicketStore->setGame($game);
            $problemTicketStore->setStore($store);
            $problemTicketStore->setVersion($version);
            $problemTicketStore->setTag($tag);
            $problemTicketStore->setMarker($marker);
            $problemTicketStore->setDevice($request->request->get("device") ? $request->request->get("device") : "");
            $problemTicketStore->setOs($request->request->get("os") ? $request->request->get("os") : "");
            $problemTicketStore->setRam($request->request->get("ram") ? $request->request->get("ram") : 0);
            $problemTicketStore->setNote($request->request->get("note") ? $request->request->get("note") : "");
            $problemTicketStore->setCreated(new \DateTime($request->request->get("created")));

            $entityManager->persist($problemTicketStore);
            $entityManager->flush();

            return new JsonResponse(
                $problemTicketStore
            );
        }
    }

    /**
     * @Route("delete/{id}/problem-ticket-store", name="delete-problem-ticket-store")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $problem_ticket_store = $entityManager->getRepository(ProblemTicketStore::class)->find($id);

        $entityManager->remove($problem_ticket_store);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-problem-tickets-store-page"
        );
    }

    /**
     * @Route("show/{page}/page/problem-ticket-store", name="show-problem-tickets-store-page")
     * @param int $page
     * @return void
     */
    public function show ($page = 0, ProblemTicketStoreModel $model)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model, $page);

        return $this->render(
            "./User/ProblemTicketStore/problem_ticket_storeList.html.twig",
            [
                "problem_tickets_store" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page,
            ]
        );
    }

    /**
     * @Route("show/{id}/problem-ticket-store", name="show-one-problem-ticket-store")
     * @param int $id
     */
    public function showOne($id = 1, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($request->request->has ('id')) {
            $ticketStoreAjax = $entityManager->getRepository (ProblemTicketStore::class)->find ($request->request->get ('id'));

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $ticketStoreAjax = $serializer->serialize($ticketStoreAjax, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return new Jsonresponse (
                $ticketStoreAjax
            );

        } else {
            $ticketStore = $entityManager->getRepository (ProblemTicketStore::class)->find ($id);

            return $this->render(
                "./User/ProblemTicketStore/problem_ticket_storeCRUD.html.twig",
                [
                    "problem_ticket_store" => $ticketStore,
                    "buttons" => "",
                ]
            );
        }
    }
}
