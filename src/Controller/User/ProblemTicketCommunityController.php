<?php

namespace App\Controller\User;

use App\Entity\ProblemTicketCommunity\ProblemTicketCommunity;
use App\Entity\ProblemTicket\Game;
use App\Entity\ProblemTicket\Marker;
use App\Entity\ProblemTicket\Server;
use App\Entity\ProblemTicket\Tag;
use App\Model\ProblemTicketCommunity\ProblemTicketCommunityModel;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\MarkerModel;
use App\Model\ProblemTicket\ServerModel;
use App\Model\ProblemTicket\TagModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProblemTicketCommunityController extends AbstractController
{
    /**
     * @Route("action/problem-ticket-community/{id}", name="action-problem-ticket-community")
     *
     * @param int|null $id
     * @param Request $request
     * @return void
     */
    public function index($id = null, Request $request, GameModel $gamesModel, ServerModel $serversModel, TagModel $tagsModel, MarkerModel $markersModel)
    {

        $entityManager = $this->getDoctrine()->getManager();

        if (!is_null($id)) {
            $problemTicketCommunity = $entityManager->getRepository(ProblemTicketCommunity::class)->find($id);

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $problemTicketCommunity = $serializer->serialize($problemTicketCommunity, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return $this->render(
                "./User/ProblemTicketCommunity/problem_ticket_community.html.twig", [
                    "games" => $gamesModel->getGames(),
                    "servers" => $serversModel->getServers(),
                    "tags" => $tagsModel->getTags(),
                    "markers" => $markersModel->getMarkers(),
                    "ticket" => $problemTicketCommunity,
                ]
            );
        } else {
            return $this->render(
                "./User/ProblemTicketCommunity/problem_ticket_community.html.twig", [
                    "games" => $gamesModel->getGames(),
                    "servers" => $serversModel->getServers(),
                    "tags" => $tagsModel->getTags(),
                    "markers" => $markersModel->getMarkers(),
                ]
            );
        }

    }

    /**
     * @Route("create/problem-ticket-community/{id}", name="create-problem-ticket-community")
     *
     * @ParamConverter("game", options={"mapping": {"game" : "id"}})
     * @ParamConverter("server", options={"mapping": {"server" : "id"}})
     * @ParamConverter("tag", options={"mapping": {"tag" : "id"}})
     * @ParamConverter("marker", options={"mapping": {"marker" : "id"}})
     */
    public function createTicket($id = null, Request $request)
    {

        if (   !$request->request->has("game")
            or !$request->request->has("server")
            or !$request->request->has("tag")
            or !$request->request->has("marker")
            or !$request->request->has("created")) {
            exit("Fileds not fillen");
        } else {

            $entityManager = $this->getDoctrine()->getManager();

            if ($id !== null && $problemTicketCommunity = $entityManager->getRepository(ProblemTicketCommunity::class)->find($id)) {
            } else {
                $problemTicketCommunity = new ProblemTicketCommunity();
            }

            $game = $request->request->has("game")
            ? $entityManager->getRepository(Game::class)->find((int) $request->request->get("game"))
            : new Game();

            $server = $request->request->has("server")
            ? $entityManager->getRepository(Server::class)->find((int) $request->request->get("server"))
            : new Server();

            $tag = $request->request->has("tag")
            ? $entityManager->getRepository(Tag::class)->find((int) $request->request->get("tag"))
            : new Tag();

            $marker = $request->request->has("marker")
            ? $entityManager->getRepository(Marker::class)->find((int) $request->request->get("marker"))
            : new Marker();

            $problemTicketCommunity->setGame($game);
            $problemTicketCommunity->setServer($server);
            $problemTicketCommunity->setPlayerId($request->request->get("player_id") ? $request->request->get("player_id") : 0);
            $problemTicketCommunity->setTag($tag);
            $problemTicketCommunity->setMarker($marker);
            $problemTicketCommunity->setNote($request->request->get("note") ? $request->request->get("note") : "");
            $problemTicketCommunity->setLink($request->request->get("link") ? $request->request->get("link") : "");
            $problemTicketCommunity->setCreated(new \DateTime($request->request->get("created")));

            $entityManager->persist($problemTicketCommunity);
            $entityManager->flush();

            return new JsonResponse(
                $problemTicketCommunity
            );
        }
    }

    /**
     * @Route("delete/{id}/problem-ticket-community", name="delete-problem-ticket-community")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $problemTicketCommunity = $entityManager->getRepository(ProblemTicketCommunity::class)->find($id);

        $entityManager->remove($problemTicketCommunity);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-problem-tickets-community-page"
        );
    }

    /**
     * @Route("show/{page}/page/problem-ticket-community", name="show-problem-tickets-community-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, ProblemTicketCommunityModel $model)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model, $page);

        return $this->render(
            "./User/ProblemTicketCommunity/problem_ticket_communityList.html.twig",
            [
                "problem_tickets_community" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page,
            ]
        );
    }

    /**
     * @Route("show/{id}/problem-ticket-community", name="show-one-problem-ticket-community")
     * @param int $id
     */
    public function showOne($id = 1, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($request->request->has ('id')) {
            $ticketCommunityAjax = $entityManager->getRepository (ProblemTicketCommunity::class)->find ($request->request->get ('id'));

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $ticketCommunityAjax = $serializer->serialize($ticketCommunityAjax, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return new Jsonresponse (
                $ticketCommunityAjax
            );

        } else {
            $ticketCommunity = $entityManager->getRepository (ProblemTicketCommunity::class)->find ($id);

            return $this->render(
                "./User/ProblemTicketCommunity/problem_ticket_communityCRUD.html.twig",
                [
                    "problem_ticket_community" => $ticketCommunity,
                    "buttons" => "",
                ]
            );
        }
    }
}
