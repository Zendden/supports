<?php

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Model\ProblemTicket\GameModel;

trait RenderOptions {
    /**
     * List of render parametrs
     * @var array $renderOptions
     */
    protected $renderOptions = [];

    /**
     * Add new option for transfer into Twig
     * 
     * @return self
     */
    public function addOption (string $option, $entity): self {
        $this->renderOptions["$option"] = $entity;
        return $this;
    }

    public function getOptions (): array {
        return $this->renderOptions;
    }
}

class MainPageController extends AbstractController {
    use RenderOptions;

    /**
     * @Route("/", name="index")
     */
    public function index(GameModel $model): Response {
        $currentDate = new \DateTime();

        $oneMonthAgo = new \DateTime();
        $oneMonthAgo->modify('-1 month');

        $twoMonthAgo = new \DateTime();
        $twoMonthAgo->modify('-2 month');

        $threeMonthAgo = new \DateTime();
        $threeMonthAgo->modify('-3 month');

        $games = $model->getGames();

        $threeToTwoMonth = $model->getGamesWithAmountFeedbackTickets([$threeMonthAgo, $twoMonthAgo]);
        $twoToOneMonth = $model->getGamesWithAmountFeedbackTickets([$twoMonthAgo, $oneMonthAgo]);
        $oneToCurrentMonth = $model->getGamesWithAmountFeedbackTickets([$oneMonthAgo, $currentDate]);

        $threeToTwoMonthStore = $model->getGamesWithAmountStoreTickets([$threeMonthAgo, $twoMonthAgo]);
        $twoToOneMonthStore = $model->getGamesWithAmountStoreTickets([$twoMonthAgo, $oneMonthAgo]);
        $oneToCurrentMonthStore = $model->getGamesWithAmountStoreTickets([$oneMonthAgo, $currentDate]);

        $asiaActiveModerators = $model->getModeratorsByServerAndStatus(1); //1 (server) - Asia
        $europeActiveModerators = $model->getModeratorsByServerAndStatus(2); //2 (server) - Europe

        $asiaModeratorsIsTimeToCheck = $model->getModeratorsByServerAndStatusWhichIsTimeToCheck(1); //Asia
        $europeModeratorsIsTimeToCheck = $model->getModeratorsByServerAndStatusWhichIsTimeToCheck(2); //Europe

        $deletedAccountWhichTimeToCheck = $model->getDeletedAccountsByGameWhichTimeToRemove();
        $deletedAccount = $model->getDeletedAccountsByGame();

        $allGamesReplace = [];
        $formedArrayWithGames = [];

        $oneToCurrentMonthReplace = [];
        $twoToOneMonthReplace = [];
        $threeToTwoMonthReplace = [];

        $oneToCurrentMonthStoreReplace = [];
        $twoToOneMonthStoreReplace = [];
        $threeToTwoMonthStoreReplace = [];

        $asiaActiveModeratorsReplace = [];
        $europeActiveModeratorsReplace = [];

        $asiaModeratorsIsTimeToCheckReplace = [];
        $europeModeratorsIsTimeToCheckReplace = [];

        $deletedAccountWhichTimeToCheckReplace = [];
        $deletedAccountReplace = [];

        /**
         * Тикеты с обратной связи
         */
        if (isset($oneToCurrentMonth)) {
            foreach ($oneToCurrentMonth as $key => $game) {
                $oneToCurrentMonthReplace[$game["id"]] = $game;
            }
        }

        if (isset($twoToOneMonth)) {
            foreach ($twoToOneMonth as $key => $game) {
                $twoToOneMonthReplace[$game["id"]] = $game;
            }
        }

        if (isset($threeToTwoMonth)) {
            foreach ($threeToTwoMonth as $key => $game) {
                $threeToTwoMonthReplace[$game["id"]] = $game;
            }
        }

        /**
         * Тикеты со сторов по периодам
         */
        if (isset($oneToCurrentMonthStore)) {
            foreach ($oneToCurrentMonthStore as $key => $game) {
                $oneToCurrentMonthStoreReplace[$game["id"]] = $game;
            }
        }

        if (isset($twoToOneMonthStore)) {
            foreach ($twoToOneMonthStore as $key => $game) {
                $twoToOneMonthStoreReplace[$game["id"]] = $game;
            }
        }

        if (isset($threeToTwoMonthStore)) {
            foreach ($threeToTwoMonthStore as $key => $game) {
                $threeToTwoMonthStoreReplace[$game["id"]] = $game;
            }
        }

        /**
         * Все активные модераторы
         */
        if (isset($asiaActiveModerators)) {
            foreach ($asiaActiveModerators as $key => $game) {
                $asiaActiveModeratorsReplace[$game["id"]] = $game;
            }
        }

        if (isset($europeActiveModerators)) {
            foreach ($europeActiveModerators as $key => $game) {
                $europeActiveModeratorsReplace[$game["id"]] = $game;
            }
        }

        /**
         * Активные модеры, которых нужно чекнуть
         */
        if (isset($asiaModeratorsIsTimeToCheck)) {
            foreach ($asiaModeratorsIsTimeToCheck as $key => $game) {
                $asiaModeratorsIsTimeToCheckReplace[$game["id"]] = $game;
            }
        }

        if (isset($europeModeratorsIsTimeToCheck)) {
            foreach ($europeModeratorsIsTimeToCheck as $key => $game) {
                $europeModeratorsIsTimeToCheckReplace[$game["id"]] = $game;
            }
        }

        /**
         * Удаленные аккаунты, пришло время удалять
         */
        if (isset($deletedAccountWhichTimeToCheck)) {
            foreach ($deletedAccountWhichTimeToCheck as $key => $game) {
                $deletedAccountWhichTimeToCheckReplace[$game["id"]] = $game;
            }
        }

        if (isset($deletedAccount)) {
            foreach ($deletedAccount as $key => $game) {
                $deletedAccountReplace[$game["id"]] = $game;
            }
        }

        /**
         * Все игры gameId => game;
         */
        if (isset($games)) {
            foreach ($games as $key => $game) {
                $allGamesReplace[$game["id"]] = $game;
            }
        }

        foreach ($allGamesReplace as $key => $game) {

            $formedArrayWithGames[$key] = [
                $game,
            ];

            if (array_key_exists($key, $oneToCurrentMonthReplace)) {
                array_push($formedArrayWithGames[$key], $oneToCurrentMonthReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $twoToOneMonthReplace)) {
                array_push($formedArrayWithGames[$key], $twoToOneMonthReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $threeToTwoMonthReplace)) {
                array_push($formedArrayWithGames[$key], $threeToTwoMonthReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }

            if (array_key_exists($key, $oneToCurrentMonthStoreReplace)) {
                array_push($formedArrayWithGames[$key], $oneToCurrentMonthStoreReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $twoToOneMonthStoreReplace)) {
                array_push($formedArrayWithGames[$key], $twoToOneMonthStoreReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $threeToTwoMonthStoreReplace)) {
                array_push($formedArrayWithGames[$key], $threeToTwoMonthStoreReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }

            if (array_key_exists($key, $asiaActiveModeratorsReplace)) {
                array_push($formedArrayWithGames[$key], $asiaActiveModeratorsReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $europeActiveModeratorsReplace)) {
                array_push($formedArrayWithGames[$key], $europeActiveModeratorsReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }

            if (array_key_exists($key, $asiaModeratorsIsTimeToCheckReplace)) {
                array_push($formedArrayWithGames[$key], $asiaModeratorsIsTimeToCheckReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $europeModeratorsIsTimeToCheckReplace)) {
                array_push($formedArrayWithGames[$key], $europeModeratorsIsTimeToCheckReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }

            if (array_key_exists($key, $deletedAccountWhichTimeToCheckReplace)) {
                array_push($formedArrayWithGames[$key], $deletedAccountWhichTimeToCheckReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
            if (array_key_exists($key, $deletedAccountReplace)) {
                array_push($formedArrayWithGames[$key], $deletedAccountReplace[$key]);
            } else {
                array_push($formedArrayWithGames[$key], []);
            }
        }

        $renderOptions = [
            "formedGames" => $formedArrayWithGames,
        ];

        $this->addOption ('formedGames', $formedArrayWithGames);

        return $this->render (
            "./User/MainPage/main.html.twig", $this->getOptions ()
        );
    }

    /**
     * @Route("/get/feedback-pie/games", name="chart-feedback-pie-by-games")
     */
    public function getFeedbackPieByGames(GameModel $model)
    {

        $currentDate = new \DateTime();
        $oneMonthAgo = new \DateTime();
        $oneMonthAgo->modify('-1 month');

        $data = $model->getGamesWithAmountFeedbackTickets([$oneMonthAgo, $currentDate]);

        return new JsonResponse(
            $data
        );
    }

    /**
     * @Route("/get/store-pie/games", name="chart-store-pie-by-games")
     */
    public function getStorePieByGames(GameModel $model)
    {

        $currentDate = new \DateTime();
        $oneMonthAgo = new \DateTime();
        $oneMonthAgo->modify('-1 month');

        $data = $model->getGamesWithAmountStoreTickets([$oneMonthAgo, $currentDate]);

        return new JsonResponse(
            $data
        );
    }

    /**
     * @Route("/get/community-pie/games", name="chart-community-pie-by-games")
     */
    public function getCommunityPieByGames(GameModel $model)
    {

        $currentDate = new \DateTime();
        $oneMonthAgo = new \DateTime();
        $oneMonthAgo->modify('-1 month');

        $data = $model->getGamesWithAmountCommunityTickets([$oneMonthAgo, $currentDate]);

        return new JsonResponse(
            $data
        );
    }
}
