<?php

namespace App\Controller\User;

use App\Model\ProblemTicketStore\StoreModel;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\PlatformModel;
use App\Model\ProblemTicket\ServerModel;
use App\Model\ProblemTicket\TagModel;
use App\Model\ProblemTicket\VersionModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ChartsStatsController extends AbstractController {
    /**
     * @var array
     */
    protected $CHARTS_OPTIONS = [];

    /**
     * @Route("/charts/show", name="show-charts")
     */
    public function show(GameModel $gameModel, VersionModel $versionModel, ServerModel $serverModel, PlatformModel $platformModel, TagModel $tagModel, StoreModel $storeModel) {
        return $this->render(
            './User/ChartsStats/charts_stats.html.twig', [
                'games' => $gameModel->getGames(),
                'versions' => $versionModel->getVersions(),
                'servers' => $serverModel->getServers(),
                'platforms' => $platformModel->getPlatforms(),
                'tags' => $tagModel->getTags(),
                'stores' => $storeModel->getStores(),
            ]
        );
    }

    /**
     * @Route("/charts/get-by/game/", name="charts-by-games")
     */
    public function getCharts(Request $request, GameModel $gameModel, VersionModel $versionModel) {
        // $data = $model->getTagsWithProblemsByGame ($id);
        $this->CHARTS_OPTIONS["location"] = $request->request->get("location") ?? exit();
        $this->CHARTS_OPTIONS["year"] = $request->request->get("year") ?? exit();
        $this->CHARTS_OPTIONS["month"] = $request->request->get("month") ?? null;
        $this->CHARTS_OPTIONS["game"] = $request->request->get("game") ?? null;
        $this->CHARTS_OPTIONS["version"] = $request->request->get("version") ?? null;
        $this->CHARTS_OPTIONS["server"] = $request->request->get("server") ?? null;
        $this->CHARTS_OPTIONS["platform"] = $request->request->get("platform") ?? null;
        $this->CHARTS_OPTIONS["store"] = $request->request->get("store") ?? null;
        $this->CHARTS_OPTIONS["tag"] = $request->request->get("tag") ?? null;

        if ($request->request->get("month") == null) {
            return new JsonResponse($gameModel->getChartDataByTicketsPerYear($this->CHARTS_OPTIONS));
        } else {
            return new JsonResponse($gameModel->getChartDataByTicketsPerMonth($this->CHARTS_OPTIONS));
        }
    }

    /**
     * @Route("/charts/get-for/pie", name="pie-chart")
     */
    public function getPieChart(Request $request, VersionModel $versionModel) {
        if ($request->request->get("pie") !== null) {
            dump ($versionModel->getPieChartByFeedbackAndStorePerYearByMonths($versionModel));
            return new JsonResponse($versionModel->getPieChartByFeedbackAndStorePerYearByMonths($versionModel));
        }
    }

}
