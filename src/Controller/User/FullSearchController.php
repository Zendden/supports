<?php

namespace App\Controller\User;

use App\Model\ProblemTicketCommunity\ProblemTicketCommunityModel;
use App\Model\ProblemTicketStore\ProblemTicketStoreModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Model\ProblemTicket\ProblemTicketModel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Model\ProblemTicketStore\StoreModel;
use App\Model\ProblemTicket\PlatformModel;
use App\Model\ProblemTicket\VersionModel;
use App\Model\ProblemTicket\ServerModel;
use App\Model\Moderator\ModeratorModel;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\TagModel;
use App\Model\Moderator\CountryModel;
use App\Traits\RenderOptionsTrait;

class FullSearchController extends Controller {
    use RenderOptionsTrait;

    protected $SEARCH_OPTIONS = [];

    /**
     * @Route("/search/", name="full-search")
     * 
     * @return Response
     */
    public function createFormSearch (
        GameModel $gamesModel,
        VersionModel $versionsModel, 
        ServerModel $serversModel, 
        PlatformModel $platformsModel, 
        TagModel $tagsModel, 
        StoreModel $storeModel,
        CountryModel $countryModel): Response {

        $this->addOption ('games', $gamesModel->getGames ());
        $this->addOption ('versions', $versionsModel->getVersions ());
        $this->addOption ('servers', $serversModel->getServers ());
        $this->addOption ('platforms', $platformsModel->getPlatforms ());
        $this->addOption ('tags', $tagsModel->getTags ());
        $this->addOption ('stores', $storeModel->getStores ());
        $this->addOption ('countries', $countryModel->getCountries ());

        return $this->render(
            './User/FullSearch/full_search.html.twig', $this->getOptions ()
        );
    }

    /**
     * @Route("/search-by/", name="search-by")
     * 
     * @return JsonResponse 
     */
    public function getListBySearch (
        Request $request,
        ModeratorModel $moderatorModel,
        ProblemTicketModel $problemTicketModel,
        ProblemTicketStoreModel $problemTicketStoreModel,
        ProblemTicketCommunityModel $problemTicketCommunityModel): JsonResponse {
            
        if ($request->request->has ('game')) {
            $this->SEARCH_OPTIONS['game'] = $request->request->get ('game');
        }
        if ($request->request->has ('version')) {
            $this->SEARCH_OPTIONS['version'] = $request->request->get ('version');
        }
        if ($request->request->has ('server')) {
            $this->SEARCH_OPTIONS['server'] = $request->request->get ('server');
        }
        if ($request->request->has ('platform')) {
            $this->SEARCH_OPTIONS['platform'] = $request->request->get ('platform');
        }
        if ($request->request->has ('tag')) {
            $this->SEARCH_OPTIONS['tag'] = $request->request->get('tag');
        }
        if ($request->request->has ('period_from') && $request->request->get ('period_from') !== ' ' && $request->request->get ('period_from') !== '') {
            $this->SEARCH_OPTIONS['period_from'] = $request->request->get ('period_from');
        }
        if ($request->request->has ('period_to')) {
            $this->SEARCH_OPTIONS['period_to'] = $request->request->get ('period_to');
        }
        if ($request->request->has ('player_id')) {
            $this->SEARCH_OPTIONS['player_id'] = $request->request->get  ('player_id');
        }
        if ($request->request->has ('link')) {
            $this->SEARCH_OPTIONS['link'] = $request->request->get ('link');
        }
        if ($request->request->has ('device')) {
            $this->SEARCH_OPTIONS['device'] = $request->request->get ('device');
        }
        if ((int) $request->request->get ('ram_from')) {
            $this->SEARCH_OPTIONS['ram_from'] = (int) $request->request->get ('ram_from');
        }
        if ((int) $request->request->get ('ram_to')) {
            $this->SEARCH_OPTIONS['ram_to'] = (int) $request->request->get ('ram_to');
        }
        if ($request->request->has ('os')) {
            $this->SEARCH_OPTIONS['os'] = $request->request->get ('os');
        }
        if ($request->request->has ('nickname')) {
            $this->SEARCH_OPTIONS['nickname'] = $request->request->get ('nickname');
        }
        if ($request->request->has ('moderator_status')) {
            $this->SEARCH_OPTIONS['moderator_status'] = $request->request->get ('moderator_status');
        }
        if ($request->request->has ('moderator_country')) {
            $this->SEARCH_OPTIONS['moderator_country'] = $request->request->get ('moderator_country');
        }
         
        if ($request->request->has ('location')) {
            if ($request->request->get ('location') === 'Feedback') {
                $searchResults = $problemTicketModel->fullSearchByFeedbackTickets ($this->SEARCH_OPTIONS);
            } elseif ($request->request->get ('location') === 'Community') {
                $searchResults = $problemTicketCommunityModel->fullSearchByCommunityTickets ($this->SEARCH_OPTIONS);
            } elseif ($request->request->get ('location') === 'Store') {
                $searchResults = $problemTicketStoreModel->fullSearchByStoreTickets ($this->SEARCH_OPTIONS);
            } elseif ($request->request->get ('location') === 'Moderators') {
                $searchResults = $moderatorModel->fullSearchByModerators ($this->SEARCH_OPTIONS);
            } else {
                exit ();
            }
        }

        return new JsonResponse ($searchResults);
    }
}