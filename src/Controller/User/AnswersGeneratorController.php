<?php

namespace App\Controller\User;

use App\Model\Generator\AnswerGeneratorModel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AnswersGeneratorController extends AbstractController {
    /**
     * @Route("/generator", name="answer-generator")
     * @return void
     */
    public function getAnswer () {
        $model = new AnswerGeneratorModel ();

        return $this->render ('./User/Generator/answer_generator.html.twig', [
            "russianPositive" => $model->createRussinPositiveAnswer (),
            "russianNetral" => $model->createRussinNetralAnswer (),
            "russianNegative" => $model->createRussinNegativeAnswer (),

            "englishPositive" => $model->createEnglishPositiveAnswer (),
            "englishNetral" => $model->createEnglishNetralAnswer (),
            "englishNegative" => $model->createEnglishNegativeAnswer ()
        ]);
    }
}