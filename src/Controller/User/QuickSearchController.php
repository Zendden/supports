<?php

namespace App\Controller\User;

use App\Entity\ProblemTicketStore\ProblemTicketStore;
use App\Entity\ProblemTicket\ProblemTicket;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class QuickSearchController extends Controller
{

    /**
     * @Route("/find-by/tickets/{id}", name="find-ticket")
     */
    public function findByTickets($id = 0)
    {
        $queryResult = $this->getDoctrine()->getRepository(ProblemTicket::class)->find($id);

        $encoders = [new JsonEncoder()];
        $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
        $serializer = new Serializer($normalizer, $encoders);

        $jsonObject = $serializer->serialize($queryResult, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
        ]);

        return new JsonResponse($jsonObject);
    }

    /**
     * @Route("/find-by/stores/{id}", name="find-store")
     */
    public function findByStores($id = 0)
    {
        $queryResult = $this->getDoctrine()->getRepository(ProblemTicketStore::class)->find($id);

        $encoders = [new JsonEncoder()];
        $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
        $serializer = new Serializer($normalizer, $encoders);

        $jsonObject = $serializer->serialize($queryResult, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
        ]);

        return new JsonResponse($jsonObject);
    }

    /**
     * @Route("/find-by/players/{id}", name="find-players")
     */
    public function findByPlayers($id = 0)
    {
        $queryResult = $this->getDoctrine()->getRepository(Players::class)->find($id);

        $encoders = [new JsonEncoder()];
        $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
        $serializer = new Serializer($normalizer, $encoders);

        $jsonObject = $serializer->serialize($queryResult, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            },
        ]);

        return new JsonResponse($jsonObject);
    }
}
