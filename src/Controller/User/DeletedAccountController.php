<?php

namespace App\Controller\User;

use App\Entity\ProblemTicket\Game;
use App\Entity\ProblemTicket\Server;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\ServerModel;
use \App\Controller\User\MainPageController;
use Symfony\Component\Serializer\Serializer;
use App\Entity\DeletedAccount\DeletedAccount;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\DeletedAccount\DeletedAccountModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DeletedAccountController extends AbstractController {
    /**
     * Indicator of checking moderator by community Team
     */
    protected $isChecked;

    /**
     * @Route("action/deleted-account/{id}", name="action-deleted-account")
     *
     * @param int|null $id
     * @return Response
     */
    public function index (int $id = null, GameModel $gamesModel, ServerModel $serversModel): Response {
        if (!is_null ($id)) {
            $entityManager = $this->getDoctrine ()->getManager ();
            $deletedAccount = $entityManager->getRepository (DeletedAccount::class)->find ($id);

            $encoders = [new JsonEncoder ()];
            $normalizer = [new ObjectNormalizer (new ClassMetadataFactory (new AnnotationLoader (new AnnotationReader ())))];
            $serializer = new Serializer ($normalizer, $encoders);

            $deletedAccount = $serializer->serialize ($deletedAccount, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId ();
                },
            ]);
        }

        $renderOptions = [
            "games" => $gamesModel->getGames (),
            "servers" => $serversModel->getServers (),
        ];

        isset ($deletedAccount) ? $renderOptions['ticket'] = $deletedAccount : null;

        return $this->render (
            "./User/DeletedAccount/deleted_account.html.twig", $renderOptions
        );
    }

    /**
     * @Route("create/deleted-account/{id}", name="create-deleted-account")
     *
     * @ParamConverter("game", options={"mapping": {"game" : "id"}})
     * @ParamConverter("server", options={"mapping": {"server" : "id"}})
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return JsonResponse
     */
    public function createTicket (int $id = null, Request $request): JsonResponse {
        if (   !$request->request->has ("game")
            or !$request->request->has ("server")
            or !$request->request->has ("player_id")
            or !$request->request->has ("link")
            or !$request->request->get ("created")) {
            exit ("Fileds not fillen");
        } else {

            $entityManager = $this->getDoctrine ()->getManager ();
            $this->isChecked = $request->request->get ("is_deleted");

            if ($id !== null && $deletedAccount = $entityManager->getRepository (DeletedAccount::class)->find ($id)) {
            } else {
                $deletedAccount = new DeletedAccount ();
            }

            $game = $request->request->has ("game")
            ? $entityManager->getRepository (Game::class)->find ((int) $request->request->get ("game"))
            : new Game ();

            $server = $request->request->has ("server")
            ? $entityManager->getRepository (Server::class)->find ((int) $request->request->get ("server"))
            : new Server ();

            $deletedAt = new \DateTime ($request->request->get ("created"));
            $deletedAt->add  (new \DateInterval ('P45D'));
            
            $deletedAccount->setGame ($game);
            $deletedAccount->setServer ($server);
            $deletedAccount->setPlayerId ($request->request->get ("player_id") ? $request->request->get ("player_id") : 0);
            $deletedAccount->setNickname ($request->request->get ("nickname") ? $request->request->get ("nickname") : "");
            $deletedAccount->setEmail ($request->request->get ("email") ? $request->request->get ("email") : "");
            $deletedAccount->setIsDeleted ($request->request->get ("is_deleted"));
            $deletedAccount->setNote ($request->request->get ("note") ? $request->request->get ("note") : "");
            $deletedAccount->setLink ($request->request->get ("link") ? $request->request->get ("link") : "");
            $deletedAccount->setDeletedAt ($deletedAt);
            $deletedAccount->setCreated (new \DateTime ($request->request->get ("created")));

            $entityManager->persist ($deletedAccount);
            $entityManager->flush ();

            return new JsonResponse (
                $deletedAccount
            );
        }
    }

    /**
     * @Route("delete/{id}/deleted-account", name="delete-deleted-account")
     * @param int $id
     * @return Response
     */
    public function delete (int $id): Response {
        $entityManager = $this->getDoctrine ()->getManager ();
        $deletedAccount = $entityManager->getRepository (DeletedAccount::class)->find ($id);
        $entityManager->remove ($deletedAccount);
        $entityManager->flush ();

        return $this->redirectToRoute (
            "show-deleted-accounts-page"
        );
    }

    /**
     * @Route("show/{page}/page/deleted-accounts", name="show-deleted-accounts-page")
     * @param int $page
     * @return Response
     */
    public function show (int $page = 0, DeletedAccountModel $model): Response {
        $puginationRows = $model->show ($page, $model);
        $puginationButtons = $model->pugination ($model);
        $counterByAllTimeToDelete = $model->countByAllTimeToDelete ();

        $renderOptions = [
            "deleted_accounts" => $puginationRows,
            "buttons" => $puginationButtons,
            "current" => $page,
            "counter_by_all_time_to_delete" => $counterByAllTimeToDelete,
        ];

        return $this->render (
            "./User/DeletedAccount/deleted_accountLIST.html.twig", $renderOptions
        );
    }

    /**
     * @Route("show/{id}/deleted-account", name="show-one-deleted-account")
     * @param int $id
     * @return Response
     */
    public function showOne (int $id = 1): Response {

        $entityManager = $this->getDoctrine ()->getManager ();
        $deletedAccount = $entityManager->getRepository (DeletedAccount::class)->find ($id);

        $renderOptions = [
            "deleted_account" => $deletedAccount,
            "buttons" => "",
        ];

        return $this->render (
            "./User/DeletedAccount/deleted_accountCRUD.html.twig", $renderOptions
        );
    }

    /**
     * @Route("show/delete-accounts-by-time/", name="deleted-accounts-by-time")
     * @param DeletedAccountModel $model
     * @return Response
     */
    public function showForDelete (DeletedAccountModel $model): Response {
        $accountsWhichIsTimeToDelete = $model->findAllIsTimeToDelete ();

        $renderOptions = [
            "accounts_which_time_to_delete" => $accountsWhichIsTimeToDelete,
        ];

        return $this->render(
            "./User/DeletedAccount/deleted_account_by_timeLIST.html.twig", $renderOptions
        );
    }
}
