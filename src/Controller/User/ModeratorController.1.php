<?php

namespace App\Controller\User;

use App\Entity\Moderator\Country;
use App\Entity\Moderator\Moderator;
use App\Entity\ProblemTicket\Game;
use App\Entity\ProblemTicket\Server;
use App\Model\Moderator\CountryModel;
use App\Model\Moderator\ModeratorModel;
use App\Model\ProblemTicket\GameModel;
use App\Model\ProblemTicket\ServerModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ModeratorController extends Controller
{
    protected $SEARCH_OPTIONS = [];
    
    /**
     * @Route("action/moderator/{id}", name="action-moderator")
     *
     * @param int|null $id
     * @param Request $request
     * @return void
     */
    public function index($id = null, Request $request, GameModel $gamesModel, ServerModel $serversModel, CountryModel $countryModel)
    {
        if ($id !== null) {
            $entityManager = $this->getDoctrine()->getManager();
            $moderator = $entityManager->getRepository(Moderator::class)->find($id);

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $moderator = $serializer->serialize($moderator, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);
        }

        return $this->render(
            "./User/Moderator/moderator.html.twig", [
                "games" => $gamesModel->getGames(),
                "servers" => $serversModel->getServers(),
                "ticket" => isset ($moderator) ? $moderator : null,
                "countries" => $countryModel->getCountries(),
            ]
        );
    }

    /**
     * @Route("create/moderator/{id}", name="create-moderator")
     *
     * @ParamConverter("game", options={"mapping": {"game" : "id"}})
     * @ParamConverter("server", options={"mapping": {"server" : "id"}})
     * @ParamConverter("country", options={"mapping": {"country" : "id"}})
     */
    public function createTicket($id = null, Request $request)
    {
        if (   !$request->request->has("game")
            or !$request->request->has("server")
            or !$request->request->has("country")
            or !$request->request->has("player_id")
            or !$request->request->has("status")
            or !$request->request->get("created")) {
            exit("Fileds not fillen");
        } else {
            if ($id !== null && $moderator = $entityManager->getRepository(Moderator::class)->find($id)) {
                $entityManager = $this->getDoctrine()->getManager();
            } else {
                $moderator = new Moderator();
            }

            $game = $request->request->has("game")
            ? $entityManager->getRepository(Game::class)->find((int) $request->request->get("game"))
            : new Game();

            $server = $request->request->has("server")
            ? $entityManager->getRepository(Server::class)->find((int) $request->request->get("server"))
            : new Server();

            $country = $request->request->has("country")
            ? $entityManager->getRepository(Country::class)->find((int) $request->request->get("country"))
            : new Country();

            $checkedAt = new \DateTime();
            $checkedDate = $checkedAt->add(new \DateInterval('P2M'));

            $moderator->setGame($game);
            $moderator->setServer($server);
            $moderator->setNickname($request->request->get("nickname") ? $request->request->get("nickname") : "");
            $moderator->setPlayerId($request->request->get("player_id") ? $request->request->get("player_id") : 0);
            $moderator->setCountry($country);
            $moderator->setStatus($request->request->get("status"));
            $moderator->setNote($request->request->get("note") ? $request->request->get("note") : "");
            $moderator->setLink($request->request->get("link") ? $request->request->get("link") : "");
            $moderator->setIsChecked($request->request->get("is_checked"));
            $moderator->setCreatedBy(!is_null($this->getUser()) ? $this->getUser() : $request->request->get("user"));
            $moderator->setCreatedAt(new \DateTime($request->request->get("created")));

            if ($request->request->has("is_checked") && $request->request->get("is_checked") == true) {
                $moderator->setCheckedAt($checkedDate);
            }

            $entityManager->persist($moderator);
            $entityManager->flush();

            return new JsonResponse(
                $moderator
            );
        }
    }

    /**
     * @Route("delete/{id}/moderator", name="delete-moderator")
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $moderator = $entityManager->getRepository(Moderator::class)->find($id);

        $entityManager->remove($moderator);
        $entityManager->flush();

        return $this->redirectToRoute(
            "show-moderators-page"
        );
    }

    /**
     * @Route("show/{page}/page/moderator", name="show-moderators-page")
     * @param int $page
     * @return void
     */
    public function show($page = 0, ModeratorModel $model, GameModel $gamesModel)
    {
        $puginationRows = $model->show($page, $model);
        $puginationButtons = $model->pugination($model);
        $moderatorsCheck = $model->countByAllTimeToCheck();
        $model->updateAllModeratorsByTime();

        return $this->render(
            "./User/Moderator/moderatorLIST.html.twig",
            [
                "games" => $gamesModel->getGames(),
                "moderators" => $puginationRows,
                "buttons" => $puginationButtons,
                "current" => $page,
                "moderators_check" => $moderatorsCheck,
            ]
        );
    }

    /**
     * @Route("show/{id}/moderator", name="show-one-moderator")
     * @param int $id
     */
    public function showOne($id = 1, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if ($request->request->has ('id')) {
            $moderatorAjax = $entityManager->getRepository (Moderator::class)->find ($request->request->get ('id'));

            $encoders = [new JsonEncoder()];
            $normalizer = [new ObjectNormalizer(new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())))];
            $serializer = new Serializer($normalizer, $encoders);

            $moderatorAjax = $serializer->serialize($moderatorAjax, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
            ]);

            return new Jsonresponse (
                $moderatorAjax
            );

        } else {
            $moderator = $entityManager->getRepository (Moderator::class)->find ($id);

            return $this->render(
                "./User/Moderator/moderatorCRUD.html.twig",
                [
                    "moderator" => $moderator,
                    "buttons" => "",
                ]
            );
        }
    }

    /**
     * @Route("show/check-moderators-by-time/", name="check-moderators-by-time")
     */
    public function showForCheck(ModeratorModel $model)
    {

        $moderatorsWhichIsTimeTocheck = $model->findAllIsTimeToCheck();

        return $this->render(
            "./User/Moderator/check_moderators_by_timeLIST.html.twig",
            [
                "moderators_which_time_to_check" => $moderatorsWhichIsTimeTocheck,
            ]
        );
    }

    /**
     * @Route("/get/moderators", name="sorted-moderator-list")
     *
     * @param Request $request
     * @return JsonResponse|null
     */
    public function showModeratorsByFilter (Request $request, ModeratorModel $moderatorModel) {
        if ($request->request->has ('project')) {
            $this->SEARCH_OPTIONS['game'] = $request->request->get ('project');
        }
        if ($request->request->has ('status')) {
            $this->SEARCH_OPTIONS['status'] = $request->request->get ('status');
        }

        if (isset ($this->SEARCH_OPTIONS['game']) || isset ($this->SEARCH_OPTIONS['status'])) {
            return new JsonResponse (
                $moderatorModel->getModeratorsByFilter ($this->SEARCH_OPTIONS)
            );
        } else {
            exit ();
        }

        return new JsonResponse (
            $moderatorModel->getModeratorsByFilter ($this->SEARCH_OPTIONS)
        );
    }
}
