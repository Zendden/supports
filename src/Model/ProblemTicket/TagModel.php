<?php

namespace App\Model\ProblemTicket;

use App\Entity\ProblemTicket\Tag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TagModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 10;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param TagModel $model
     * @return array
     */
    public function show($page, TagModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param TagModel $model
     * @return array
     */
    public function pugination(TagModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("Tags")
            ->select("count(Tags)")
            ->where("Tags.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    public function getTags(): array
    {

        return $this->createQueryBuilder("t")
            ->select("t.id, t.description")
            ->getQuery()
            ->getResult();
    }

    public function getTagsWithProblemsByGame($gameId)
    {
        if (!is_null($gameId)) {
            $data = $this->createQueryBuilder("t")
                ->select([
                    't.id',
                    'game.id AS gameId',
                    "t.title AS title",
                    'COUNT(pt.id) AS cnt',
                    'MONTH(pt.created) AS createdAt',
                ])
                ->innerJoin("t.problemTicket", "pt")
                ->innerJoin("pt.game", "game")
                ->groupBy("t.id, game.id, createdAt")
                ->where("game.id = :gId")
                ->setParameter("gId", $gameId)
                ->andWhere('pt.created >= :per')
                ->setParameter("per", (new \DateTime())->modify('-3 year'))
                ->getQuery()
                ->getResult();

            $output = [];
            foreach ($data as $row) {
                $output[$row["title"]]["name"] = $row['title'];
                $output[$row["title"]]["data"][] = [(int) ($row['createdAt'] - 1), (int) $row['cnt']];
            }
            dump($output);
            return $output;
        }

    }
}
// public function getTagsWithProblemsByGame ($gameId): array {
//     // if (!is_null ($gameId)) {

//     // }
//     $data = $this->createQueryBuilder ("t")
//         ->select ([
//             't.id',
//             'game.id AS gameId',
//             "CONCAT(t.title, '-', game.title) AS title",
//             'COUNT(pt.id) AS cnt',
//             'WEEK(pt.created, 0) AS week',
//         ])
//         ->innerJoin ("t.problemTicket", "pt")
//         ->innerJoin ("pt.game", "game")
//         ->groupBy ("t.id, game.id, week")
//         // ->where ("gameId == :game")
//         // ->setParameter ("game", $game)
//         ->getQuery ()
//         ->getResult ();

//     $output = [];
//     foreach ($data as $row) {
//         $output[$row["title"]]['name'] = $row['title'];
//         $output[$row["title"]]['data'][] = [
//             (int) $row['week'],
//             (int) $row['cnt'],
//         ];
//     }

//     return $output;
// }
