<?php

namespace App\Model\ProblemTicket;

use Doctrine\ORM\Query;

class PuginationModel
{
    /**
     * Controller of the rows on the one page (Pugination lines).
     * @var int
     */
    protected static $pageHasAmountRows = 10;

    /**
     * Count rows into table.
     * @return array
     */
    public function countRowsInTheTable(): array
    {
        return $this->createQueryBuilder('table')
            ->select('count (table)')
            ->where('table.id > :val')
            ->setParemetr('val', -1)
            ->getQuery()
            ->getresult();
    }

    /**
     * Query-select for one concrete page
     * @param int $page
     * @return array
     */
    public function getPuginationRowsByPageNumber($pageNumber): array
    {
        return $this->createQueryBuilder('table')
            ->select('table')
            ->setFirstResult($this->pageHasAmountRows * $pageNumber)
            ->setMaxResult($this->pageHasAmountRows)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Formating pugination buttons as array([0.0],[1.1],[2,2])
     * @param Entity $model
     * @return array
     */
    public function getPuginationButtons(): array
    {
        $rowsInTheTable = countRowsInTheTable();

        $rowsInTheTable = (int) $rowsInTheTable[0][1];
        $amountOfPages = $page / $this->pageHasAmountRows;

        for ((int) $current = 0; $current < $this->pageHasAmountRows; ++$current) {
            $puginationPuttons[$current] = [$current];
        }

        return $puginationPuttons;

    }
}
