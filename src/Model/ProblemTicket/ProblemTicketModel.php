<?php

namespace App\Model\ProblemTicket;

use App\Entity\ProblemTicket\ProblemTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProblemTicketModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 25;

    protected $searchRows = 25;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProblemTicket::class);
    }

    /**
     * List with ProblemTicket by pugination
     *
     * @param int $page
     * @param ProblemTicketModel $model
     * @return array
     */
    public function show($page, ProblemTicketModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param ProblemTicketModel $model
     * @return array
     */
    public function pugination (ProblemTicketModel $model, $currentPage): array {
        $pages = $model->countRows ();
        $pages = (int) $pages[0][1];

        $previousPages = $currentPage - 3;
        $afterPages = $currentPage + 3;

        $puginationButtons = array ();
        $amountPages = round ($pages / $this->amountRows);

        if ($previousPages <= 0) {
            $previousPages = 0;
        }
        if ($afterPages >= $amountPages) {
            $afterPages = $amountPages;
        }

        for ($i = 0; $i < $amountPages; $i++) {
            if ($i >= $previousPages && $i <= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                $puginationButtons[$i] = [null];
            }

            if ($i <= $afterPages && $i >= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                continue;
            }
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("problem_tickets")
            ->select("count(problem_tickets)")
            ->where("problem_tickets.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for search by id in tickets
     *
     * @return array
     */
    public function findByConcreteTicket($id): array
    {

        return $this->createQueryBuilder("p")
            ->select("p.id, p.player_id, p.note, p.link, p.created, g.id AS Game, s.id AS Server, pl.id AS platform, v.id AS version, t.id AS tag, m.id AS marker")
            ->leftJoin('p.game', 'g')
            ->leftJoin('p.server', 's')
            ->leftJoin('p.platform', 'pl')
            ->leftJoin('p.version', 'v')
            ->leftJoin('p.tag', 't')
            ->leftJoin('p.marker', 'm')
            ->where("p.id = :val")
            ->setParameter("val", $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for search by all fields
     *
     * @return array
     */
    public function fullSearchByFeedbackTickets($SEARCH_OPTIONS): array
    {
        $qb = $this->createQueryBuilder("p");
        $qb->select("p.id, p.player_id, p.created, g.title AS game, s.title AS server, pl.title AS platform, v.title AS version, t.description AS tag, m.description AS marker, p.note AS note")
            ->leftJoin('p.game', 'g')
            ->leftJoin('p.server', 's')
            ->leftJoin('p.platform', 'pl')
            ->leftJoin('p.version', 'v')
            ->leftJoin('p.marker', 'm')
            ->leftJoin('p.tag', 't');

        foreach ($SEARCH_OPTIONS as $field => $value) {
            if (!is_null($value)) {
                if ($field == "game") {
                    $qb->where("p.game = :val_game")
                        ->setParameter("val_game", $value);
                }
                if ($field == "version") {
                    $qb->andWhere("p.version = :val_version")
                        ->setParameter("val_version", $value);
                }
                if ($field == "server") {
                    $qb->andWhere("p.server = :val_server")
                        ->setParameter("val_server", $value);
                }
                if ($field == "platform") {
                    $qb->andWhere("p.platform = :val_platform")
                        ->setParameter("val_platform", $value);
                }
                if ($field == "tag") {
                    $qb->andWhere("p.tag = :val_tag")
                        ->setParameter("val_tag", $value);
                }
                if ($field == "period_from") {
                    $qb->andWhere("p.created >= :per_from")
                        ->setParameter("per_from", new \DateTime($value));
                }
                if ($field == "period_to") {
                    $qb->andWhere("p.created <= :per_to")
                        ->setParameter("per_to", new \DateTime($value));
                }
                if ($field == "player_id") {
                    $qb->andWhere("p.player_id LIKE :player_id_val")
                        ->setParameter("player_id_val", '%' . $value . '%');
                }
                if ($field == "link") {
                    $qb->andWhere("p.link LIKE :ticket_link_val")
                        ->setParameter("ticket_link_val", '%' . $value . '%');
                }
            }
        }

        $qb->orderBy ('p.created', 'DESC');

        $queryResult = $qb->getQuery()->getArrayResult();

        return $queryResult;
    }
}
