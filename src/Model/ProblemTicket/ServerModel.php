<?php

namespace App\Model\ProblemTicket;

use App\Entity\ProblemTicket\Server;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ServerModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 10;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Server::class);
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param ServerModel $model
     * @return array
     */
    public function show($page, ServerModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param ServerModel $model
     * @return array
     */
    public function pugination(ServerModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("Servers")
            ->select("count(Servers)")
            ->where("Servers.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    public function getServers(): array
    {

        return $this->createQueryBuilder("s")
            ->select("s.id, s.title")
            ->getQuery()
            ->getResult();
    }
}
