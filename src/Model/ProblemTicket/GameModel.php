<?php

namespace App\Model\ProblemTicket;

use App\Entity\ProblemTicket\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class GameModel extends ServiceEntityRepository
{

    protected $current_month;
    protected $current_date;
    protected $yearChartCategories = [
        0 => "January",
        1 => "February",
        2 => "March",
        3 => "April",
        4 => "May",
        5 => "June",
        6 => "July",
        7 => "August",
        8 => "September",
        9 => "October",
        10 => "November",
        11 => "December",
    ];

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 10;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Game::class);

        $this->current_date = new \DateTime();
        $this->first_day_of_current_month = new \DateTime();
        $this->first_day_of_current_month->modify('-1 month');
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param GameModel $model
     * @return array
     */
    public function show($page, GameModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param GameModel $model
     * @return array
     */
    public function pugination(GameModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("Games")
            ->select("count(Games)")
            ->where("Games.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    public function getGames(): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title")
            ->getQuery()
            ->getResult();
    }

    public function getGamesWithAmountFeedbackTickets($period): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(p.id) AS y")
            ->groupBy("g.id")
            ->leftJoin("g.problemTicket", "p")
            ->where("p.created >= :per_from")
            ->setParameter("per_from", $period[0])
            ->andWhere("p.created <= :per_to")
            ->setParameter("per_to", $period[1])
            ->getQuery()
            ->getResult();
    }

    public function getGamesWithAmountStoreTickets($period): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(p.id) AS y")
            ->groupBy("g.id")
            ->leftJoin("g.problemTicketStore", "p")
            ->where("p.created >= :per_from")
            ->setParameter("per_from", $period[0])
            ->andWhere("p.created <= :per_to")
            ->setParameter("per_to", $period[1])
            ->getQuery()
            ->getResult();
    }

    public function getGamesWithAmountCommunityTickets($period): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(p.id) AS y")
            ->groupBy("g.id")
            ->leftJoin("g.problemTicketCommunity", "p")
            ->where("p.created >= :per_from")
            ->setParameter("per_from", $period[0])
            ->andWhere("p.created <= :per_to")
            ->setParameter("per_to", $period[1])
            ->getQuery()
            ->getResult();
    }

    public function getModeratorsByServerAndStatus($server): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(m.id) AS moderators")
            ->groupBy("g.id")
            ->leftJoin("g.moderator", "m")
            ->where("m.status = :status AND m.server = :server")
            ->setParameter("status", "Действующий")
            ->setParameter("server", $server)
            ->getQuery()
            ->getResult();
    }

    public function getModeratorsByServerAndStatusWhichIsTimeToCheck($server): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(m.id) AS moderators")
            ->groupBy("g.id")
            ->leftJoin("g.moderator", "m")
            ->where("m.status = :status AND m.server = :server")
            ->setParameter("status", "Действующий")
            ->setParameter("server", $server)
            ->andWhere("m.checkedAt <= :val")
            ->setParameter("val", new \DateTime())
            ->getQuery()
            ->getResult();
    }

    public function getDeletedAccountsByGame(): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(d.id) AS deletedAccounts")
            ->groupBy("g.id")
            ->leftJoin("g.deletedAccount", "d")
            ->andWhere("d.isDeleted = 0")
            ->getQuery()
            ->getResult();
    }

    public function getDeletedAccountsByGameWhichTimeToRemove(): array
    {

        return $this->createQueryBuilder("g")
            ->select("g.id, g.title AS name, count(d.id) AS deletedAccounts")
            ->groupBy("g.id")
            ->leftJoin("g.deletedAccount", "d")
            ->where("d.deletedAt <= :val")
            ->setParameter("val", new \DateTime())
            ->andWhere("d.isDeleted = 0")
            ->getQuery()
            ->getResult();
    }

    public function getChartDataByTicketsPerYear($CHARTS_OPTIONS): array
    {
        $query = $this->createQueryBuilder("g");

        foreach ($CHARTS_OPTIONS as $field => $value) {
            if (!is_null($value)) {
                if ($field === "location") {
                    if ($value === "Feedback") {
                        $query->innerJoin("g.problemTicket", "pt");
                    } elseif ($value === "Community") {
                        $query->innerJoin("g.problemTicketCommunity", "pt");
                    } elseif ($value === "Store") {
                        $query->innerJoin("g.problemTicketStore", "pt");
                    }

                    $query->select([
                        'g.title',
                        'COUNT(pt.id) AS cnt',
                        'MONTH(pt.created) AS createdAtMonth',
                    ]);
                }

                if ($field === "game") {
                    $query->andWhere("g.id = :gameId")
                        ->setParameter("gameId", $value);
                }

                if ($CHARTS_OPTIONS["location"] !== "Community") {
                    if ($field === "version") {
                        $query->andWhere("pt.version = :versionId")
                            ->setParameter("versionId", $value);
                    }
                }

                if ($CHARTS_OPTIONS["location"] !== "Store") {
                    if ($field === "server") {
                        $query->andWhere("pt.server = :serverId")
                            ->setParameter("serverId", $value);
                    }
                } else {
                    if ($field === "store") {
                        $query->andWhere("pt.store = :storeId")
                            ->setParameter("storeId", $value);
                    }
                }

                if ($CHARTS_OPTIONS["location"] === "Feedback") {
                    if ($field === "platform") {
                        $query->andWhere("pt.platform = :platformId")
                            ->setParameter("platformId", $value);
                    }
                }

                if ($field === "tag") {
                    $query->andWhere("pt.tag = :tagId")
                        ->setParameter("tagId", $value);
                }

                if ($field === "year") {
                    $from = (new \DateTime(date($value) . "-01-01"))->format("Y-m-d");
                    $to = (new \DateTime(date($value) . "-12-31"))->format("Y-m-d");
                    $year = $value;

                    $query->andWhere("pt.created >= :from")
                        ->setParameter("from", $from);
                    $query->andWhere("pt.created <= :to")
                        ->setParameter("to", $to);
                }
            }
        }

        if (!empty($query)) {
            $query->groupBy("g.title, createdAtMonth");
            $data = $query->getQuery()->getArrayResult();
        }

        $output = [];
        foreach ($data as $row) {
            $title = $row['title'];
            $month = (int) $row['createdAtMonth'] - 1;

            $output["projects"][$title]["name"] = $title;

            if (!isset($output["projects"][$title]['data'])) {
                $output["projects"][$title]['data'] = array_fill(0, 12, null);
                $output["projects"][$title]["data"][$month] = (int) $row['cnt'];
            } else {
                $output["projects"][$title]["data"][$month] += (int) $row['cnt'];
            }
        }
        $output["charts"]["categories"] = $this->yearChartCategories;

        return $output;
    }

    public function getChartDataByTicketsPerMonth($CHARTS_OPTIONS): array
    {
        $query = $this->createQueryBuilder("g");

        foreach ($CHARTS_OPTIONS as $field => $value) {
            if (!is_null($value)) {
                if ($field === "location") {
                    if ($value === "Feedback") {
                        $query->innerJoin("g.problemTicket", "pt");
                    } elseif ($value === "Community") {
                        $query->innerJoin("g.problemTicketCommunity", "pt");
                    } elseif ($value === "Store") {
                        $query->innerJoin("g.problemTicketStore", "pt");
                    }

                    $query->select([
                        'g.title',
                        'COUNT(pt.id) AS cnt',
                        'DAY(pt.created) AS createdAtDay',
                    ]);
                }

                if ($field === "game") {
                    $query->andWhere("g.id = :gameId")
                        ->setParameter("gameId", $value);
                }

                if ($CHARTS_OPTIONS["location"] !== "Community") {
                    if ($field === "version") {
                        $query->andWhere("pt.version = :versionId")
                            ->setParameter("versionId", $value);
                    }
                }

                if ($CHARTS_OPTIONS["location"] !== "Store") {
                    if ($field === "server") {
                        $query->andWhere("pt.server = :serverId")
                            ->setParameter("serverId", $value);
                    }
                } else {
                    if ($field === "store") {
                        $query->andWhere("pt.store = :storeId")
                            ->setParameter("storeId", $value);
                    }
                }

                if ($CHARTS_OPTIONS["location"] === "Feedback") {
                    if ($field === "platform") {
                        $query->andWhere("pt.platform = :platformId")
                            ->setParameter("platformId", $value);
                    }
                }

                if ($field === "tag") {
                    $query->andWhere("pt.tag = :tagId")
                        ->setParameter("tagId", $value);
                }

                if ($field === "year") {
                    $year = $value;
                }
                if ($field === "month") {
                    $from = (new \DateTime(date($year) . "-" . $value . "-01"))->format("Y-m-d");
                    $to = (new \DateTime(date($year) . "-" . $value . "-31"))->format("Y-m-d");
                    $amountDaysInChoosenMonth = 1 + cal_days_in_month(CAL_GREGORIAN, $value, $year);

                    $query->andWhere("pt.created >= :from")
                        ->setParameter("from", $from);
                    $query->andWhere("pt.created <= :to")
                        ->setParameter("to", $to);
                }

            }
        }

        if (!empty($query)) {
            $query->groupBy("g.title, createdAtDay");
            $data = $query->getQuery()->getArrayResult();
        }

        $output = [];
        foreach ($data as $row) {
            $title = $row['title'];
            $day = (int) $row['createdAtDay'];

            $output["projects"][$title]["name"] = $title;

            if (!isset($output["projects"][$title]['data'])) {
                $output["projects"][$title]['data'] = array_fill(0, $amountDaysInChoosenMonth, null);
                $output["projects"][$title]['data'][(int) $day] = (int) $row['cnt'];
            } else {
                $output["projects"][$title]['data'][(int) $day] += (int) $row['cnt'];
            }
        }
        $output["charts"]["categories"] = array_fill(0, $amountDaysInChoosenMonth, null);

        return $output;
    }
}
