<?php

namespace App\Model\ProblemTicket;

use App\Entity\ProblemTicket\GameVersion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class VersionModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 10;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameVersion::class);
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param VersionModel $model
     * @return array
     */
    public function show($page, VersionModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param VersionModel $model
     * @return array
     */
    public function pugination(VersionModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("GameVersion")
            ->select("count(GameVersion)")
            ->where("GameVersion.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    public function getVersions(): array
    {

        return $this->createQueryBuilder("v")
            ->select("v.id, v.title, g.id AS game")
            ->leftJoin('v.game', 'g')
            ->orderBy('v.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getPieChartByStorePerYearByMonths()
    {
        return $this->createQueryBuilder("v")
            ->select("v.id AS versionId, v.title AS version, g.title AS game, COUNT(st.id) AS ticketsStore")
            ->leftJoin("v.game", "g")
            ->leftJoin("v.problemTicketStore", "st")
            // ->andWhere("st.created >= :from")
            // ->setParameter("from", new \Datetime(date('d/m/y', strtotime('first day of January', time()))))
            // ->andWhere("st.created <= :to")
            // ->setParameter("to", new \Datetime())
            ->groupBy('game, versionId, version')
            ->getQuery()
            ->getArrayResult();
    }

    public function getPieChartByFeedbackAndStorePerYearByMonths(VersionModel $versionModel)
    {
        $feedback = $this->createQueryBuilder("v")
            ->select("v.id AS versionId, v.title AS version, g.title AS game, COUNT(pt.id) AS tickets")
            ->leftJoin("v.game", "g")
            ->leftJoin("v.problemTicket", "pt")
            // ->andWhere("pt.created >= :from")
            // ->setParameter("from", new \Datetime(date('d/m/y', strtotime('first day of January', time()))))
            // ->andWhere("pt.created <= :to")
            // ->setParameter("to", new \Datetime())
            ->groupBy('game, versionId, version')
            ->getQuery()
            ->getArrayResult();

        $store = $versionModel->getPieChartByStorePerYearByMonths();

        $output = [];
        foreach ($feedback as $row) {
            $game = $row['game'];
            $version = $row['version'];
            $numTickets = $row['tickets'];

            if (!isset ($output['projects'][$game][$version]["feedback"])) {
                $output['projects'][$game][$version]["feedback"] = (int) $numTickets;
            } else {
                $output['projects'][$game][$version]["feedback"] += (int) $numTickets;
            }
        }
        foreach ($store as $row) {
            $game = $row['game'];
            $version = $row['version'];
            $numTicketsStore = $row['ticketsStore'];

            if (!isset ($output['projects'][$game][$version]["store"])) {
                $output['projects'][$game][$version]["store"] = (int) $numTicketsStore;
            } else {
                $output['projects'][$game][$version]["store"] += (int) $numTicketsStore;
            }
        }
        
        dump ($output);
        $ticketsByAllVersions = [];
        $formedOutput = [
            'name' => '',
            'data' => [],
            'drilldown' => [],
        ];
        foreach ($output['projects'] as $gameTitle => $versions) {
            foreach ($versions as $versionByGameTitle => $versionByGame) {
                if (!isset($ticketsByAllVersions[$gameTitle])) {
                    $ticketsByAllVersions[$gameTitle] = $output['projects'][$gameTitle][$versionByGameTitle]['feedback'] ?? 0 + $output['projects'][$gameTitle][$versionByGameTitle]['store'] ?? 0;

                } else {
                    $ticketsByAllVersions[$gameTitle] = $ticketsByAllVersions[$gameTitle] + ($output['projects'][$gameTitle][$versionByGameTitle]['feedback'] ?? 0 + $output['projects'][$gameTitle][$versionByGameTitle]['store'] ?? 0);
                }

                if (!isset($formedOutput['data'][$gameTitle])) {
                    $formedOutput['data'][$gameTitle] = [
                        'name' => $gameTitle,
                        'y' => (int) ($ticketsByAllVersions[$gameTitle]),
                        'drilldown' => $gameTitle,
                    ];

                    $formedOutput['drilldown'][$gameTitle] = [
                        'name' => $gameTitle,
                        'id' => $gameTitle,
                    ];
                } else {
                    $formedOutput['data'][$gameTitle]['y'] = $formedOutput['data'][$gameTitle]['y'] + (int) ($ticketsByAllVersions[$gameTitle]);
                }
            }
            foreach ($versions as $versionTitle => $version) {
                $formedOutput['drilldown'][$gameTitle]['data'][] = [$versionTitle, ($version['feedback'] ?? 0 + $version['store'] ?? 0)];
            }
        }

        $drilldown = [];
        foreach ($formedOutput['drilldown'] as $item) {
            $drilldown[] = $item;
        }

        $series = [];
        foreach ($formedOutput['data'] as $game) {
            $series[] = $game;
        }

        return [[
            'series' => [[
                'name' => 'Pie Chart by Versions',
                'data' => $series,
            ]],
            'drilldown' => [
                'series' => $drilldown,
            ],
        ]];
    }
}
