<?php

namespace App\Model\Generator;

class AnswerGeneratorModel {
    /** @var string */
    private $file; #читаем весь файл
    
	/** @var int*/
    private $counter; #счетчик выхода
    
	/** @var string */
    private $body; #название читаемого файла
    
	/** @var string */
	private $answerString; #выход контейнер

    public function createAnswer (string $chooseFile) {
        
        $this->body = $chooseFile;
        $this->file = file ($this->body);
        $this->counter = count ($this->file)-1;
        $this->answerString = $this->file[rand(0, $this->counter)];

        if (is_string ($this->answerString)) {
        	return $this->answerString;
        }
    }


    public function createRussinPositiveAnswer () {
        (string) $rusPositive = "";

        $rusPositive .= $this->createAnswer (__DIR__ ."/"."ru/data_hi_ru.txt");
        $rusPositive .= $this->createAnswer (__DIR__ ."/"."ru/data_body_one_ru.txt");
        $rusPositive .= $this->createAnswer (__DIR__ ."/"."ru/data_body_two_ru.txt");
        $rusPositive .= $this->createAnswer (__DIR__ ."/"."ru/data_end_ru.txt");

        return $rusPositive;
    }

    public function createRussinNetralAnswer () {
        (string) $rusNetral = "";
        
        $rusNetral .= $this->createAnswer (__DIR__ ."/"."ru/data_hi_ru.txt");
        $rusNetral .= $this->createAnswer (__DIR__ ."/"."ru/data_body_netral_ru.txt");
        $rusNetral .= $this->createAnswer (__DIR__ ."/"."ru/data_end_ru.txt");

        return $rusNetral;
    }

    public function createRussinNegativeAnswer () {
        (string) $rusNegative = "";
        
        $rusNegative .= $this->createAnswer (__DIR__ ."/"."ru/data_hi_ru.txt");
        $rusNegative .= $this->createAnswer (__DIR__ ."/"."ru/data_body_negative_ru.txt");
        $rusNegative .= $this->createAnswer (__DIR__ ."/"."ru/data_end_ru.txt");

        return $rusNegative;
    }


    public function createEnglishPositiveAnswer () {
        (string) $engPositive = "";

        $engPositive .= $this->createAnswer (__DIR__ ."/"."en/data_hi_en.txt");
        $engPositive .= $this->createAnswer (__DIR__ ."/"."en/data_body_one_en.txt");
        $engPositive .= $this->createAnswer (__DIR__ ."/"."en/data_body_two_en.txt");
        $engPositive .= $this->createAnswer (__DIR__ ."/"."en/data_end_en.txt");

        return $engPositive;
    }

    public function createEnglishNetralAnswer () {
        (string) $engNetral = "";
        
        $engNetral .= $this->createAnswer (__DIR__ ."/"."en/data_hi_en.txt");
        $engNetral .= $this->createAnswer (__DIR__ ."/"."en/data_body_netral_en.txt");
        $engNetral .= $this->createAnswer (__DIR__ ."/"."en/data_end_en.txt");

        return $engNetral;
    }

    public function createEnglishNegativeAnswer () {
        (string) $engNegative = "";
        
        $engNegative .= $this->createAnswer (__DIR__ ."/"."en/data_hi_en.txt");
        $engNegative .= $this->createAnswer (__DIR__ ."/"."en/data_body_negative_en.txt");
        $engNegative .= $this->createAnswer (__DIR__ ."/"."en/data_end_en.txt");

        return $engNegative;
    }
}