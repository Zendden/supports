<?php

namespace App\Model\ProblemTicketStore;

use App\Entity\ProblemTicketStore\ProblemTicketStore;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProblemTicketStoreModel extends ServiceEntityRepository
{
    /**
     * Amount rows by one page.
     *
     * @var int
     */
    protected $amountRows = 25;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProblemTicketStore::class);
    }

    /**
     * List with Games by pugination.
     *
     * @param int                     $page
     * @param ProblemTicketStoreModel $model
     *
     * @return array
     */
    public function show($page, ProblemTicketStoreModel $model): array
    {
        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons.
     *
     * @param ProblemTicketStoreModel $model
     *
     * @return array
     */
    public function pugination (ProblemTicketStoreModel $model, $currentPage): array
    {
        $pages = $model->countRows();
        $pages = (int) $pages[0][1];

        $previousPages = $currentPage - 3;
        $afterPages = $currentPage + 3;

        $puginationButtons = array();
        $amountPages = round ($pages / $this->amountRows);

        if ($previousPages <= 0) {
            $previousPages = 0;
        }
        if ($afterPages >= $amountPages) {
            $afterPages = $amountPages;
        }

        for ($i = 0; $i < $amountPages; $i++) {
            if ($i >= $previousPages && $i <= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                $puginationButtons[$i] = [null];
            }

            if ($i <= $afterPages && $i >= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                continue;
            }
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination.
     *
     * @param int $page
     *
     * @return array
     */
    public function findByPugination($page): array
    {
        return $this->createQueryBuilder('p')
            ->select('p')
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table.
     *
     * @return array
     */
    public function countRows(): array
    {
        return $this->createQueryBuilder('ProblemTicketStore')
            ->select('count(ProblemTicketStore)')
            ->where('ProblemTicketStore.id > :val')
            ->setParameter('val', -1)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for search by all fields.
     *
     * @return array
     */
    public function fullSearchByStoreTickets($SEARCH_OPTIONS): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select('p.id, p.device, p.os, p.ram, p.created, g.title AS game, s.title AS store, v.title AS version, t.description AS tag, m.description AS marker')
            ->leftJoin('p.game', 'g')
            ->leftJoin('p.store', 's')
            ->leftJoin('p.version', 'v')
            ->leftJoin('p.marker', 'm')
            ->leftJoin('p.tag', 't');

        foreach ($SEARCH_OPTIONS as $field => $value) {
            if (!is_null($value)) {
                if ($field == 'game') {
                    $qb->where('p.game = :val_game')
                        ->setParameter('val_game', $value);
                }
                if ($field == 'version') {
                    $qb->andWhere('p.version = :val_version')
                        ->setParameter('val_version', $value);
                }
                if ($field == 'store') {
                    $qb->andWhere('p.store = :val_store')
                        ->setParameter('val_store', $value);
                }
                if ($field == 'tag') {
                    $qb->andWhere('p.tag = :val_tag')
                        ->setParameter('val_tag', $value);
                }
                if ($field == 'period_from') {
                    $qb->andWhere('p.created >= :per_from')
                        ->setParameter('per_from', new \DateTime($value));
                }
                if ($field == 'period_to') {
                    $qb->andWhere('p.created <= :per_to')
                        ->setParameter('per_to', new \DateTime($value));
                }
                if ($field == 'device') {
                    $qb->andWhere('p.device LIKE :device_val')
                        ->setParameter('device_val', '%' . $value . '%');
                }
                if ($field == 'os') {
                    $qb->andWhere('p.os LIKE :os_val')
                        ->setParameter('os_val', '%' . $value . '%');
                }
                if ($field == 'ram_from') {
                    $qb->andWhere('p.ram >= :ram_from_val')
                        ->setParameter('ram_from_val', (int) $value);
                }
                if ($field == 'ram_to') {
                    $qb->andWhere('p.ram <= :ram_to_val')
                        ->setParameter('ram_to_val', (int) $value);
                }
            }
        }

        $qb->orderBy ('p.created', 'DESC');

        $queryResult = $qb->getQuery()->getArrayResult();

        return $queryResult;
    }
}
