<?php

namespace App\Model\ProblemTicketStore;

use App\Entity\ProblemTicketStore\Store;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class StoreModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 10;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Store::class);
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param StoreModel $model
     * @return array
     */
    public function show($page, StoreModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param StoreModel $model
     * @return array
     */
    public function pugination(StoreModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("Store")
            ->select("count(Store)")
            ->where("Store.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function getStores(): array
    {

        return $this->createQueryBuilder("s")
            ->select("s.id, s.title")
            ->getQuery()
            ->getResult();
    }
}
