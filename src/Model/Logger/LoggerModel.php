<?php

namespace App\Model\Logger;

use App\Entity\Logger\Logger;
use Doctrine\ORM\EntityManagerInterface;

class LoggerModel
{

    protected $user; ## username

    protected $type; ## feedbacks, stores, deletedAccounts, communityTickets, moderators

    protected $group; ## create, edit, delete, search, show

    protected $originalDataObject; ## before changing data

    protected $changedDataObject; ## changed data

    protected $created; ## time of created a new log

    protected $logger; ## logger entity

    public function __construct($type, $group, $user, Logger $logger)
    {
        $this->type = (string) $type;
        $this->group = (string) $group;
        $this->user = (string) $user;
        $this->created = new \DateTime();
        $this->logger = $logger;
    }

    public function writeToLogOrigin($originalDataObject): void
    {
        if (is_string($this->type) && is_string($this->group)) {
            $this->logger->setType($this->type);
            $this->logger->setGroup($this->group);
            $this->logger->setUser($this->user);
            $this->logger->setCreated($this->created);

            if (!is_null($originalDataObject)) {
                $this->logger->setDataObject($originalDataObject);
            }
        }
    }

    public function writeToLogModified($changedDataObject): void
    {
        if (!is_null($changedDataObject)) {
            $this->logger->setChangedDataObject($changedDataObject);
        }
    }

    public function persistLogger(EntityManagerInterface $entityManager): void
    {
        if ($this->logger) {
            $entityManager->persist($this->logger);
        }
    }
}
