<?php

namespace App\Model\ProblemTicketCommunity;

use App\Entity\ProblemTicketCommunity\ProblemTicketCommunity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProblemTicketCommunityModel extends ServiceEntityRepository
{

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 25;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProblemTicketCommunity::class);
    }

    /**
     * List with ProblemTicket by pugination
     *
     * @param int $page
     * @param ProblemTicketCommunityModel $model
     * @return array
     */
    public function show($page, ProblemTicketCommunityModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param ProblemTicketCommunityModel $model
     * @return array
     */
    public function pugination(ProblemTicketCommunityModel $model, $currentPage): array
    {
        $pages = $model->countRows();
        $pages = (int) $pages[0][1];

        $previousPages = $currentPage - 3;
        $afterPages = $currentPage + 3;

        $puginationButtons = array();
        $amountPages = round ($pages / $this->amountRows);

        if ($previousPages <= 0) {
            $previousPages = 0;
        }
        if ($afterPages >= $amountPages) {
            $afterPages = $amountPages;
        }

        for ($i = 0; $i < $amountPages; $i++) {
            if ($i >= $previousPages && $i <= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                $puginationButtons[$i] = [null];
            }

            if ($i <= $afterPages && $i >= $currentPage) {
                $puginationButtons[$i] = [$i];
            } else {
                continue;
            }
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("p")
            ->select("count(p)")
            ->where("p.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

/**
 * Query for search by all fields
 *
 * @return array
 */
    public function fullSearchByCommunityTickets($SEARCH_OPTIONS): array
    {

        $qb = $this->createQueryBuilder("p");
        $qb->select("p.id, p.player_id, p.created, g.title AS game, s.title AS server, t.description AS tag, m.description AS marker")
            ->leftJoin('p.game', 'g')
            ->leftJoin('p.server', 's')
            ->leftJoin('p.marker', 'm')
            ->leftJoin('p.tag', 't');

        foreach ($SEARCH_OPTIONS as $field => $value) {
            if (!is_null($value)) {
                if ($field == "game") {
                    $qb->where("p.game = :val_game")
                        ->setParameter("val_game", $value);
                }
                if ($field == "server") {
                    $qb->andWhere("p.server = :val_server")
                        ->setParameter("val_server", $value);
                }
                if ($field == "tag") {
                    $qb->andWhere("p.tag = :val_tag")
                        ->setParameter("val_tag", $value);
                }
                if ($field == "period_from") {
                    $qb->andWhere("p.created >= :per_from")
                        ->setParameter("per_from", new \DateTime($value));
                }
                if ($field == "period_to") {
                    $qb->andWhere("p.created <= :per_to")
                        ->setParameter("per_to", new \DateTime($value));
                }
                if ($field == "player_id") {
                    $qb->andWhere("p.player_id LIKE :player_id_val")
                        ->setParameter("player_id_val", '%' . $value . '%');
                }
                if ($field == "link") {
                    $qb->andWhere("p.link LIKE :ticket_link_val")
                        ->setParameter("ticket_link_val", '%' . $value . '%');
                }
            }

            $qb->orderBy ('p.created', 'DESC');
        }

        $queryResult = $qb->getQuery()->getArrayResult();

        return $queryResult;
    }
}
