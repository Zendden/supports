<?php

namespace App\Model\Moderator;

use App\Entity\Moderator\Moderator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ModeratorModel extends ServiceEntityRepository {
    /**
     * Current date
     * @var \DateTime $currentDate
     */
    protected $currentDate;

    /**
     * Amount rows by one page
     * @var int $amountRows
     */
    protected $amountRows = 25;

    public function __construct (RegistryInterface $registry) {
        parent::__construct ($registry, Moderator::class);
        $this->currentDate = new \DateTime ();
    }

    /**
     * Array with Games by pugination
     * @param int $page
     * @param ModeratorModel $model
     * @return array
     */
    public function show ($page, ModeratorModel $model): array{
        $listArray = $model->findByPugination ($page);
        return $listArray;
    }

    /**
     * Array with pugination Buttons
     * @param ModeratorModel $model
     * @return array
     */
    public function pugination (ModeratorModel $model): array {
        $pages = $model->countRows ();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array ();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     * @param int $page
     * @return array
     */
    public function findByPugination ($page): array {
        return $this->createQueryBuilder ("m")
            ->select ("m")
            ->setFirstResult ($this->amountRows * $page)
            ->setMaxResults ($this->amountRows)
            ->orderBy ("m.id", "DESC")
            ->getQuery ()
            ->getResult ();
    }

    /**
     * Query for amount rows in a table
     * @return array
     */
    public function countRows (): array {
        return $this->createQueryBuilder ("m")
            ->select ("count(m)")
            ->where ("m.id > :val")
            ->setParameter ("val", -1)
            ->getQuery ()
            ->getResult ();
    }

    /**
     * Query for amount rows in Checked Accounts
     */
    public function findAllIsTimeToCheck (): array {
        return $this->createQueryBuilder ("m")
            ->select ("m")
            ->andWhere ("m.checkedAt <= :val")
            ->setParameter ("val", $this->currentDate)
            ->andWhere ("m.status = :status")
            ->setParameter ("status", 'Действующий')
            ->getQuery ()
            ->getResult ();
    }

    /**
     * Query for amount rows in Checked Accounts which need to check
     */
    public function countByAllTimeToCheck (): array {
        return $this->createQueryBuilder ("m")
            ->select ("count(m)")
            ->andWhere ("m.checkedAt <= :val")
            ->setParameter ("val", $this->currentDate)
            ->andWhere ("m.status = :status")
            ->setParameter ("status", 'Действующий')
            ->getQuery ()
            ->getResult ();
    }

    public function updateAllModeratorsByTime (): void {
        $this->createQueryBuilder ('m')
            ->update ()
            ->set ('m.isChecked', 0)
            ->where ("m.checkedAt <= :val")
            ->setParameter ("val", $this->currentDate)
            ->getQuery ()
            ->execute ();
    }

    public function fullSearchByModerators ($SEARCH_OPTIONS): array {
        $qb = $this->createQueryBuilder ('m');
        $qb->select ('m.id, g.title AS game, s.title AS server, m.nickname, m.player_id, c.title AS country, m.status, m.status_weight, m.isChecked, m.createdBy, m.checkedAt')
           ->leftJoin ('m.game', 'g')
           ->leftJoin ('m.server', 's')
           ->leftJoin ('m.country', 'c');
        
        foreach ($SEARCH_OPTIONS as $field => $value) {
            if (!is_null ($value)) {
                if ($field == "game") {
                    $qb->where ("m.game = :val_game")
                        ->setParameter ("val_game", $value);
                }
                if ($field == "server") {
                    $qb->andWhere ("m.server = :val_server")
                        ->setParameter ("val_server", $value);
                }
                if ($field == "player_id") {
                    $qb->andWhere ("m.player_id LIKE :player_id_val")
                        ->setParameter ("player_id_val", '%' . $value . '%');
                }
                if ($field == "moderator_status") {
                    $qb->andWhere ("m.status LIKE :moderator_status_val")
                        ->setParameter ("moderator_status_val", $value);
                }
                if ($field == "nickname") {
                    $qb->andWhere ("m.nickname LIKE :nickname_val")
                        ->setParameter ("nickname_val", '%' . $value . '%');
                }
                if ($field == "moderator_country") {
                    $qb->andWhere ("c.id = :country_val")
                        ->setParameter ("country_val", $value);
                }
            }
        }

        $queryResult = $qb->getQuery ()->getArrayResult ();

        return $queryResult;
    } 

    public function getModeratorsByFilter ($SEARCH_OPTIONS): array {
        $qb = $this->createQueryBuilder ('m')
                    ->select ('m AS moderator, g.title AS game, s.title AS server, c.title AS country')
                    ->leftJoin ('m.game', 'g')
                    ->leftJoin ('m.server', 's')
                    ->leftJoin ('m.country', 'c');

            foreach ($SEARCH_OPTIONS as $field => $value) {
                if ($field === 'game') {
                    $qb->andWhere ('g.id = :game_val')
                        ->setParameter ('game_val', $value);
                }
                if ($field === 'status') {
                    $qb->andWhere ('m.status = :status_val')
                        ->setParameter ('status_val', (string) $value);
                }
            }

            $queryResult = $qb->getQuery ()->getArrayResult ();

            return $queryResult;
    }
}
