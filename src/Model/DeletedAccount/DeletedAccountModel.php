<?php

namespace App\Model\DeletedAccount;

use App\Entity\DeletedAccount\DeletedAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class DeletedAccountModel extends ServiceEntityRepository
{

    protected $currentDate;

    /**
     * Amount rows by one page
     *
     * @var int
     */
    protected $amountRows = 20;

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DeletedAccount::class);
        $this->currentDate = new \DateTime();
    }

    /**
     * List with Games by pugination
     *
     * @param int $page
     * @param DeletedAccountModel $model
     * @return array
     */
    public function show($page, DeletedAccountModel $model): array
    {

        $listArray = $model->findByPugination($page);

        return $listArray;
    }

    /**
     * List with pugination Buttons
     *
     * @param DeletedAccountModel $model
     * @return array
     */
    public function pugination(DeletedAccountModel $model): array
    {

        $pages = $model->countRows();

        $pages = $pages[0][1];
        $pages = (int) $pages;
        $puginationButtons = array();
        $amountPages = ($pages / $this->amountRows);

        for ($i = 0; $i < $amountPages; $i++) {
            $puginationButtons[$i] = [$i];
        }

        return $puginationButtons;
    }

    /**
     * Query for pugination
     *
     * @param int $page
     * @return array
     */
    public function findByPugination($page): array
    {

        return $this->createQueryBuilder("p")
            ->select("p")
            ->setFirstResult($this->amountRows * $page)
            ->setMaxResults($this->amountRows)
            ->orderBy('p.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in a table
     *
     * @return array
     */
    public function countRows(): array
    {

        return $this->createQueryBuilder("d")
            ->select("count(d)")
            ->where("d.id > :val")
            ->setParameter("val", -1)
            ->getQuery()
            ->getResult();
    }

    /**
     * Query for amount rows in Deleted Accounts
     */
    public function findAllIsTimeToDelete(): array
    {

        return $this->createQueryBuilder("d")
            ->select("d")
            ->andWhere("d.deletedAt <= :val")
            ->setParameter("val", $this->currentDate)
            ->andWhere("d.isDeleted = 0")
            ->getQuery()
            ->getResult();

    }

    /**
     * Query for amount rows in Deleted Accounts which need deleted
     */
    public function countByAllTimeToDelete(): array
    {

        return $this->createQueryBuilder("d")
            ->select("count(d)")
            ->where("d.deletedAt <= :val")
            ->andWhere("d.isDeleted = 0")
            ->setParameter("val", $this->currentDate)
            ->getQuery()
            ->getResult();
    }
}
