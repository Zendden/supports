<?php

namespace App\Entity\Moderator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="countrys")
 */
class Country
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @ORM\OneToMany (targetEntity="Moderator", mappedBy="country")
     */
    protected $moderator;

    public function __construct()
    {
        $this->moderators = new ArrayCollection();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getModerators()
    {
        return $this->moderator;
    }

    /**
     * Setters
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $title
     */
    public function setModerators($moderator): void
    {
        $this->moderator = $moderator;
    }
}
