<?php

namespace App\Entity\Moderator;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="moderators")
 */
class Moderator
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Game", inversedBy="moderator")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Game|null
     */
    protected $game;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Server", inversedBy="moderator")
     * @ORM\JoinColumn (name="server", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Server|null
     */
    protected $server;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Blank
     */
    protected $nickname;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank
     */
    protected $player_id;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="moderator")
     * @Assert\NotBlank
     */
    protected $country;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank
     */
    protected $status;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank
     */
    protected $status_weight;

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\Blank
     */
    protected $note;

    /**
     * @ORM\Column(type="string")
     * @Assert\Blank
     * @var string
     */
    protected $link;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $isChecked;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $createdBy;

    /**
     * @ORM\Column(name="checked", type="datetime", nullable=false)
     * @Assert\NotBlank
     * @var \DateTime
     */
    protected $checkedAt;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Assert\NotBlank
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->checkedAt = new \DateTime();
        $this->createdAt = new \DateTime();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?\App\Entity\ProblemTicket\Game
    {
        return $this->game;
    }

    public function getServer(): ?\App\Entity\ProblemTicket\Server
    {
        return $this->server;
    }

    public function getPlayerId()
    {
        return $this->player_id;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusWeight ()
    {
        return $this->status_weight;
    }

    public function getIsChecked(): bool
    {
        return $this->isChecked;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getCheckedAt(): \DateTime
    {
        return $this->checkedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Setters
     */
    public function setGame(?\App\Entity\ProblemTicket\Game $game): self
    {
        $this->game = $game;
        return $this;
    }

    public function setServer(?\App\Entity\ProblemTicket\Server $server): self
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @param integer $player_id
     */
    public function setPlayerId($player_id): void
    {
        $this->player_id = $player_id;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname): void
    {
        $this->nickname = $nickname;
    }

    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @param string $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;

        if ($status == "Действующий") {
            $this->status_weight = 1;
        } elseif ($status == "Ожидает") {
            $this->status_weight = 2;
        } elseif ($status == "Сам ушел") {
            $this->status_weight = 3;
        } elseif ($status == "Отклонен") {
            $this->status_weight = 4;
        } elseif ($status == "Изгнан") {
            $this->status_weight = 5;
        }
    }

    public function setStatusWeight (): void
    {
    }

    public function setIsChecked($isChecked): void
    {
        $this->isChecked = (bool) $isChecked;
    }

    /**
     * @param string $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    public function setCreatedBy($createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @param \DateTime $checkedAt
     */
    public function setCheckedAt($checkedAt): void
    {
        $this->checkedAt = $checkedAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

}
