<?php

namespace App\Entity\ProblemTicketCommunity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="problem_tickets_community")
 */
class ProblemTicketCommunity
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Game", inversedBy="problemTicketCommunity")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Game
     */
    protected $game;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Server", inversedBy="problemTicketCommunity")
     * @ORM\JoinColumn (name="server", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Server
     */
    protected $server;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Tag", inversedBy="problemTicketCommunity")
     * @ORM\JoinColumn (name="tag", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Tag
     */
    protected $tag;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Marker", inversedBy="problemTicketCommunity")
     * @ORM\JoinColumn (name="marker", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Marker
     */
    protected $marker;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Blank
     * 
     * @var int|null
     */
    protected $player_id;

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\Blank
     * 
     * @var string|null
     */
    protected $note;

    /**
     * @ORM\Column(type="string")
     * @Assert\Blank
     * 
     * @var string|null
     */
    protected $link;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \DateTime
     */
    protected $created;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame()
    {
        return $this->game;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getPlayerId()
    {
        return $this->player_id;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getMarker()
    {
        return $this->marker;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * Setters
     * @param string $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }

    public function setServer($server): void
    {
        $this->server = $server;
    }

    /**
     * @param integer $player_id
     */
    public function setPlayerId($player_id): void
    {
        $this->player_id = $player_id;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    public function setMarker($marker): void
    {
        $this->marker = $marker;
    }

    /**
     * @param string $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

}
