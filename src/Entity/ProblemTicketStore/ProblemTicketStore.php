<?php

namespace App\Entity\ProblemTicketStore;

use App\Entity\ProblemTicket\Tag;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="problem_tickets_store")
 */
class ProblemTicketStore
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Game", inversedBy="problemTicketStore")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Game
     */
    protected $game;

    /**
     * @ORM\ManyToOne (targetEntity="Store", inversedBy="problemTicketStore")
     * @ORM\JoinColumn (name="store", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicketStore\Store
     */
    protected $store;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\GameVersion", inversedBy="problemTicketStore")
     * @ORM\JoinColumn (name="version", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\GameVersion
     */
    protected $version;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Tag", inversedBy="problemTicketStore")
     * @ORM\JoinColumn (name="tag", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Tag
     */
    protected $tag;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Marker", inversedBy="problemTicketStore")
     * @ORM\JoinColumn (name="marker", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \App\Entity\ProblemTicket\Marker
     */
    protected $marker;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Blank
     * 
     * @var string|null
     */
    protected $os;

    /**
     * @ORM\Column(type="integer", length=50)
     * @Assert\Blank
     *
     * @var int|null
     */
    protected $ram;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Blank
     * 
     * @var string|null
     */
    protected $device;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Assert\Blank
     * 
     * @var string|null
     */
    protected $note;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Assert\NotBlank
     * 
     * @var \DateTime
     */
    protected $created;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame()
    {
        return $this->game;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function getStore()
    {
        return $this->store;
    }

    public function getOs()
    {
        return $this->os;
    }

    public function getRam(): int
    {
        return (int) $this->ram;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getMarker()
    {
        return $this->marker;
    }

    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setGame($game): void
    {
        $this->game = $game;
    }

    public function setDevice($device): void
    {
        $this->device = $device;
    }

    public function setStore($store): void
    {
        $this->store = $store;
    }

    /**
     * @param string $os
     */
    public function setOs($os): void
    {
        $this->os = $os;
    }

    /**
     * @param int $ram
     */
    public function setRam($ram): void
    {
        $this->ram = (int) $ram;
    }

    public function setVersion($version): void
    {
        $this->version = $version;
    }

    /**
     * @param Tag $tag
     */
    public function setTag(Tag $tag): void
    {
        $this->tag = $tag;
    }

    public function setMarker($marker): void
    {
        $this->marker = $marker;
    }

    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

}
