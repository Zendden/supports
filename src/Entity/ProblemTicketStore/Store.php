<?php

namespace App\Entity\ProblemTicketStore;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="stores")
 */
class Store
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @ORM\OneToMany(targetEntity="ProblemTicketStore", mappedBy="store")
     */
    protected $title;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicketStore", mappedBy="store")
     */
    private $problemTicketStore;

    public function __construct()
    {
        $this->problemTicketStore = new ArrayCollection();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Setters
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}
