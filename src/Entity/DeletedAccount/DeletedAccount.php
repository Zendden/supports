<?php

namespace App\Entity\DeletedAccount;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="deleted_accounts")
 */
class DeletedAccount
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Game", inversedBy="deletedAccount")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     *
     * @var \App\Entity\ProblemTicket\Game|null
     */
    protected $game;

    /**
     * @ORM\ManyToOne (targetEntity="\App\Entity\ProblemTicket\Server", inversedBy="deletedAccount")
     * @ORM\JoinColumn (name="server", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     *
     * @var \App\Entity\ProblemTicket\Server|null
     */
    protected $server;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Blank
     *
     * @var string|null
     */
    protected $nickname;

    /**
     * @ORM\Column(type="string", length=200)
     * @Assert\Blank
     *
     * @var string|null
     */
    protected $email;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     *
     * @var int
     */
    protected $player_id;

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\Blank
     *
     * @var string|null
     */
    protected $note;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     *
     * @var string
     */
    protected $link;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     * @Assert\NotBlank
     *
     * @var bool
     */
    protected $isDeleted = false;

    /**
     * @ORM\Column(name="deleted", type="datetime", nullable=false)
     * @Assert\NotBlank
     *
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Assert\NotBlank
     *
     * @var \DateTime
     */
    protected $created;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->deletedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?\App\Entity\ProblemTicket\Game
    {
        return $this->game;
    }

    public function getServer(): ?\App\Entity\ProblemTicket\Server
    {
        return $this->server;
    }

    public function getPlayerId(): ?int
    {
        return $this->player_id;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setGame(?\App\Entity\ProblemTicket\Game $game): self
    {
        $this->game = $game;
        return $this;
    }

    public function setServer(?\App\Entity\ProblemTicket\Server $server): self
    {
        $this->server = $server;
        return $this;
    }

    public function setPlayerId(int $player_id): self
    {
        $this->player_id = $player_id;
        return $this;
    }

    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;
        return $this;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = (bool) $isDeleted;
        return $this;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;
        return $this;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;
        return $this;
    }

    public function setDeletedAt(\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;
        return $this;
    }

}
