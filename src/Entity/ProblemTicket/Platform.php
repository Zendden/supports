<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="platforms")
 */
class Platform
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $title;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="platform")
     */
    private $problemTicket;

    public function __construct()
    {
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketStore = new ArrayCollection();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Setters
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}
