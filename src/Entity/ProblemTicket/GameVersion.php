<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="game_versions")
 */
class GameVersion
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @ORM\ManyToOne (targetEntity="Game", inversedBy="versions")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     */
    private $game;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="version")
     */
    private $problemTicket;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketStore\ProblemTicketStore", mappedBy="version")
     */
    private $problemTicketStore;

    public function __construct()
    {
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketStore = new ArrayCollection();
    }

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getGame()
    {
        return $this->game;
    }

    /**
     * Setters
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @param float $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }
}
