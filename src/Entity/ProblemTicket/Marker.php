<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="markers")
 */
class Marker
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    protected $description;

    /**
     * @ORM\ManyToOne (targetEntity="Tag", inversedBy="marker")
     * @ORM\JoinColumn (name="tag_id", referencedColumnName="id", nullable=false)
     */
    private $tag;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="marker")
     */
    private $problemTicket;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketStore\ProblemTicketStore", mappedBy="marker")
     */
    private $problemTicketStore;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketCommunity\ProblemTicketCommunity", mappedBy="marker")
     */
    private $problemTicketCommunity;

    public function __construct()
    {
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketStore = new ArrayCollection();
        $this->problemTicketCommunity = new ArrayCollection();
    }

    /**
     * Getters
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Setters
     *
     * @param string $gameTitle
     * @return void
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $tag
     * @return void
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }
}
