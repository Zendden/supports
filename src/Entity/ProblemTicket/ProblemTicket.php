<?php

namespace App\Entity\ProblemTicket;

use App\Entity\ProblemTicket\GameVersion;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="problem_tickets")
 */
class ProblemTicket
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne (targetEntity="Game", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="game", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $game;

    /**
     * @ORM\ManyToOne (targetEntity="Server", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="server", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $server;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Blank
     */
    protected $player_id;

    /**
     * @ORM\ManyToOne (targetEntity="Platform", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="platform", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $platform;

    /**
     * @ORM\ManyToOne (targetEntity="GameVersion", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="version", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $version;

    /**
     * @ORM\ManyToOne (targetEntity="Tag", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="tag", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $tag;

    /**
     * @ORM\ManyToOne (targetEntity="Marker", inversedBy="problemTicket")
     * @ORM\JoinColumn (name="marker", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank
     */
    protected $marker;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Assert\Blank
     */
    protected $note;

    /**
     * @ORM\Column(type="string")
     * @Assert\Blank
     * @var string
     */
    protected $link;

    /**
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Assert\NotBlank
     * @var \DateTime
     */
    protected $created;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    /**
     * Getters
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame()
    {
        return $this->game;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getPlayerId()
    {
        return $this->player_id;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getMarker()
    {
        return $this->marker;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * Setters
     * @param string $game
     */
    public function setGame($game): void
    {
        $this->game = $game;
    }

    public function setServer($server): void
    {
        $this->server = $server;
    }

    /**
     * @param integer $player_id
     */
    public function setPlayerId($player_id): void
    {
        $this->player_id = $player_id;
    }

    /**
     * @param string $platform
     */
    public function setPlatform($platform): void
    {
        $this->platform = $platform;
    }

    public function setVersion(GameVersion $version): void
    {
        $this->version = $version;
    }

    /**
     * @param string $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    public function setMarker($marker): void
    {
        $this->marker = $marker;
    }

    /**
     * @param string $note
     */
    public function setNote($note): void
    {
        $this->note = $note;
    }

    /**
     * @param string $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }

}
