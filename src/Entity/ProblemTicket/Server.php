<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="servers")
 */
class Server
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $title;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="server")
     */
    private $problemTicket;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketCommunity\ProblemTicketCommunity", mappedBy="server")
     */
    private $problemTicketCommunity;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\DeletedAccount\DeletedAccount", mappedBy="server")
     */
    private $deletedAccount;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Moderator\Moderator", mappedBy="server")
     */
    protected $moderator;

    public function __construct()
    {
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketCommunity = new ArrayCollection();
        $this->deletedAccount = new ArrayCollection();
    }

    /**
     * Getters
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Setters
     * @param string|null $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}
