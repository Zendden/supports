<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="games")
 */
class Game
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @ORM\OneToMany (targetEntity="GameVersion", mappedBy="game")
     */
    protected $versions;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="game")
     */
    protected $problemTicket;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketStore\ProblemTicketStore", mappedBy="game")
     */
    protected $problemTicketStore;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketCommunity\ProblemTicketCommunity", mappedBy="game")
     */
    private $problemTicketCommunity;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\DeletedAccount\DeletedAccount", mappedBy="game")
     */
    private $deletedAccount;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\Moderator\Moderator", mappedBy="game")
     */
    private $moderator;

    public function __construct()
    {
        $this->versions = new ArrayCollection();
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketStore = new ArrayCollection();
        $this->problemTicketCommunity = new ArrayCollection();
        $this->deletedAccount = new ArrayCollection();
    }

    /**
     * Getters
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getVersions(): PersistentCollection
    {
        return $this->versions;
    }

    /**
     * Setters
     *
     * @param string $gameTitle
     * @return void
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $version
     * @return void
     */
    public function setVersion($versions): void
    {
        $this->versions[] = $versions;
    }
}
