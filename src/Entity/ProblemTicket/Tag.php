<?php

namespace App\Entity\ProblemTicket;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="tags")
 */
class Tag
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="Marker", mappedBy="tag")
     */
    protected $marker;

    /**
     * @ORM\OneToMany (targetEntity="ProblemTicket", mappedBy="tag")
     */
    protected $problemTicket;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketStore\ProblemTicketStore", mappedBy="tag")
     */
    protected $problemTicketStore;

    /**
     * @ORM\OneToMany (targetEntity="\App\Entity\ProblemTicketCommunity\ProblemTicketCommunity", mappedBy="tag")
     */
    private $problemTicketCommunity;

    public function __construct()
    {
        $this->marker = new ArrayCollection();
        $this->problemTicket = new ArrayCollection();
        $this->problemTicketStore = new ArrayCollection();
        $this->problemTicketCommunity = new ArrayCollection();
    }

    /**
     * Getter
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getMarker(): PersistentCollection
    {
        return $this->marker;
    }

    public function getProblemTicketStore(): PersistentCollection
    {
        return $this->problemTicketStore;
    }

    /**
     * Setters
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return void
     */
    public function setMarker($marker): void
    {
        $this->marker[] = $marker;
    }
}
