<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805064854 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE logger');
        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE is_deleted is_deleted TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE moderators ADD status_weight INT NOT NULL, CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT NOT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE server server INT NOT NULL, CHANGE platform platform INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL, CHANGE game game INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT NOT NULL, CHANGE store store INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('UPDATE moderators SET status_weight = 1 WHERE status = "Действующий"');
        $this->addSql('UPDATE moderators SET status_weight = 2 WHERE status = "Ожидает"');
        $this->addSql('UPDATE moderators SET status_weight = 3 WHERE status = "Сам ушел"');
        $this->addSql('UPDATE moderators SET status_weight = 4 WHERE status = "Отклонен"');
        $this->addSql('UPDATE moderators SET status_weight = 5 WHERE status = "Изгнан"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE logger (id INT AUTO_INCREMENT NOT NULL, user VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, type VARCHAR(35) NOT NULL COLLATE utf8mb4_unicode_ci, `group` VARCHAR(35) NOT NULL COLLATE utf8mb4_unicode_ci, data_object JSON NOT NULL, changed_data_object JSON DEFAULT NULL, created DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE is_deleted is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT DEFAULT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE moderators DROP status_weight, CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE platform platform INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT DEFAULT NULL, CHANGE store store INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
    }
}
