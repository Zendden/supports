<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190815142034 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tag_markers');
        $this->addSql('DROP TABLE tags_store');
        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('ALTER TABLE moderators ADD status_weight INT NOT NULL, CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT NOT NULL, CHANGE store store INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL, CHANGE note note VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT NOT NULL');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE platform platform INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL, CHANGE note note VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE is_deleted is_deleted TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tag_markers (marker_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_73B8F29ABAD26311 (tag_id), INDEX IDX_73B8F29A474460EB (marker_id), PRIMARY KEY(marker_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE tags_store (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE tag_markers ADD CONSTRAINT FK_73B8F29A474460EB FOREIGN KEY (marker_id) REFERENCES markers (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE tag_markers ADD CONSTRAINT FK_73B8F29ABAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE is_deleted is_deleted TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT DEFAULT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE moderators DROP status_weight, CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE platform platform INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL, CHANGE note note VARCHAR(500) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT DEFAULT NULL, CHANGE store store INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL, CHANGE note note VARCHAR(500) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
