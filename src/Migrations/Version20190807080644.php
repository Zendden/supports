<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190807080644 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE countrys (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE moderators (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, server INT NOT NULL, country_id INT DEFAULT NULL, nickname VARCHAR(100) NOT NULL, player_id INT NOT NULL, status VARCHAR(255) NOT NULL, status_weight INT NOT NULL, note VARCHAR(500) NOT NULL, link VARCHAR(255) NOT NULL, is_checked TINYINT(1) NOT NULL, created_by VARCHAR(255) NOT NULL, checked DATETIME NOT NULL, created DATETIME NOT NULL, INDEX IDX_580D16D3232B318C (game), INDEX IDX_580D16D35A6DD5F6 (server), INDEX IDX_580D16D3F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stores (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE problem_tickets_store (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, store INT NOT NULL, version INT NOT NULL, tag INT NOT NULL, marker INT NOT NULL, os VARCHAR(50) NOT NULL, ram INT NOT NULL, device VARCHAR(100) NOT NULL, note VARCHAR(500) NOT NULL, created DATETIME NOT NULL, INDEX IDX_C4C7D1DB232B318C (game), INDEX IDX_C4C7D1DBFF575877 (store), INDEX IDX_C4C7D1DBBF1CD3C3 (version), INDEX IDX_C4C7D1DB389B783 (tag), INDEX IDX_C4C7D1DB82CF20FE (marker), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE deleted_accounts (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, server INT NOT NULL, nickname VARCHAR(100) NOT NULL, email VARCHAR(200) NOT NULL, player_id INT NOT NULL, note VARCHAR(500) NOT NULL, link VARCHAR(255) NOT NULL, is_deleted TINYINT(1) DEFAULT \'0\' NOT NULL, deleted DATETIME NOT NULL, created DATETIME NOT NULL, INDEX IDX_1944EDED232B318C (game), INDEX IDX_1944EDED5A6DD5F6 (server), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE platforms (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE markers (id INT AUTO_INCREMENT NOT NULL, tag_id INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(500) NOT NULL, INDEX IDX_4189DF30BAD26311 (tag_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE problem_tickets (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, server INT NOT NULL, platform INT NOT NULL, version INT NOT NULL, tag INT NOT NULL, marker INT NOT NULL, player_id INT NOT NULL, note VARCHAR(500) NOT NULL, link VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_B2645D87232B318C (game), INDEX IDX_B2645D875A6DD5F6 (server), INDEX IDX_B2645D873952D0CB (platform), INDEX IDX_B2645D87BF1CD3C3 (version), INDEX IDX_B2645D87389B783 (tag), INDEX IDX_B2645D8782CF20FE (marker), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE servers (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE games (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE game_versions (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, title VARCHAR(255) NOT NULL, INDEX IDX_B298BFF4232B318C (game), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE problem_tickets_community (id INT AUTO_INCREMENT NOT NULL, game INT NOT NULL, server INT NOT NULL, tag INT NOT NULL, marker INT NOT NULL, player_id INT NOT NULL, note VARCHAR(500) NOT NULL, link VARCHAR(255) NOT NULL, created DATETIME NOT NULL, INDEX IDX_3273A5BF232B318C (game), INDEX IDX_3273A5BF5A6DD5F6 (server), INDEX IDX_3273A5BF389B783 (tag), INDEX IDX_3273A5BF82CF20FE (marker), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE moderators ADD CONSTRAINT FK_580D16D3232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE moderators ADD CONSTRAINT FK_580D16D35A6DD5F6 FOREIGN KEY (server) REFERENCES servers (id)');
        $this->addSql('ALTER TABLE moderators ADD CONSTRAINT FK_580D16D3F92F3E70 FOREIGN KEY (country_id) REFERENCES countrys (id)');
        $this->addSql('ALTER TABLE problem_tickets_store ADD CONSTRAINT FK_C4C7D1DB232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE problem_tickets_store ADD CONSTRAINT FK_C4C7D1DBFF575877 FOREIGN KEY (store) REFERENCES stores (id)');
        $this->addSql('ALTER TABLE problem_tickets_store ADD CONSTRAINT FK_C4C7D1DBBF1CD3C3 FOREIGN KEY (version) REFERENCES game_versions (id)');
        $this->addSql('ALTER TABLE problem_tickets_store ADD CONSTRAINT FK_C4C7D1DB389B783 FOREIGN KEY (tag) REFERENCES tags (id)');
        $this->addSql('ALTER TABLE problem_tickets_store ADD CONSTRAINT FK_C4C7D1DB82CF20FE FOREIGN KEY (marker) REFERENCES markers (id)');
        $this->addSql('ALTER TABLE deleted_accounts ADD CONSTRAINT FK_1944EDED232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE deleted_accounts ADD CONSTRAINT FK_1944EDED5A6DD5F6 FOREIGN KEY (server) REFERENCES servers (id)');
        $this->addSql('ALTER TABLE markers ADD CONSTRAINT FK_4189DF30BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D87232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D875A6DD5F6 FOREIGN KEY (server) REFERENCES servers (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D873952D0CB FOREIGN KEY (platform) REFERENCES platforms (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D87BF1CD3C3 FOREIGN KEY (version) REFERENCES game_versions (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D87389B783 FOREIGN KEY (tag) REFERENCES tags (id)');
        $this->addSql('ALTER TABLE problem_tickets ADD CONSTRAINT FK_B2645D8782CF20FE FOREIGN KEY (marker) REFERENCES markers (id)');
        $this->addSql('ALTER TABLE game_versions ADD CONSTRAINT FK_B298BFF4232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE problem_tickets_community ADD CONSTRAINT FK_3273A5BF232B318C FOREIGN KEY (game) REFERENCES games (id)');
        $this->addSql('ALTER TABLE problem_tickets_community ADD CONSTRAINT FK_3273A5BF5A6DD5F6 FOREIGN KEY (server) REFERENCES servers (id)');
        $this->addSql('ALTER TABLE problem_tickets_community ADD CONSTRAINT FK_3273A5BF389B783 FOREIGN KEY (tag) REFERENCES tags (id)');
        $this->addSql('ALTER TABLE problem_tickets_community ADD CONSTRAINT FK_3273A5BF82CF20FE FOREIGN KEY (marker) REFERENCES markers (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE moderators DROP FOREIGN KEY FK_580D16D3F92F3E70');
        $this->addSql('ALTER TABLE problem_tickets_store DROP FOREIGN KEY FK_C4C7D1DBFF575877');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D873952D0CB');
        $this->addSql('ALTER TABLE problem_tickets_store DROP FOREIGN KEY FK_C4C7D1DB82CF20FE');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D8782CF20FE');
        $this->addSql('ALTER TABLE problem_tickets_community DROP FOREIGN KEY FK_3273A5BF82CF20FE');
        $this->addSql('ALTER TABLE moderators DROP FOREIGN KEY FK_580D16D35A6DD5F6');
        $this->addSql('ALTER TABLE deleted_accounts DROP FOREIGN KEY FK_1944EDED5A6DD5F6');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D875A6DD5F6');
        $this->addSql('ALTER TABLE problem_tickets_community DROP FOREIGN KEY FK_3273A5BF5A6DD5F6');
        $this->addSql('ALTER TABLE problem_tickets_store DROP FOREIGN KEY FK_C4C7D1DB389B783');
        $this->addSql('ALTER TABLE markers DROP FOREIGN KEY FK_4189DF30BAD26311');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D87389B783');
        $this->addSql('ALTER TABLE problem_tickets_community DROP FOREIGN KEY FK_3273A5BF389B783');
        $this->addSql('ALTER TABLE moderators DROP FOREIGN KEY FK_580D16D3232B318C');
        $this->addSql('ALTER TABLE problem_tickets_store DROP FOREIGN KEY FK_C4C7D1DB232B318C');
        $this->addSql('ALTER TABLE deleted_accounts DROP FOREIGN KEY FK_1944EDED232B318C');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D87232B318C');
        $this->addSql('ALTER TABLE game_versions DROP FOREIGN KEY FK_B298BFF4232B318C');
        $this->addSql('ALTER TABLE problem_tickets_community DROP FOREIGN KEY FK_3273A5BF232B318C');
        $this->addSql('ALTER TABLE problem_tickets_store DROP FOREIGN KEY FK_C4C7D1DBBF1CD3C3');
        $this->addSql('ALTER TABLE problem_tickets DROP FOREIGN KEY FK_B2645D87BF1CD3C3');
        $this->addSql('DROP TABLE countrys');
        $this->addSql('DROP TABLE moderators');
        $this->addSql('DROP TABLE stores');
        $this->addSql('DROP TABLE problem_tickets_store');
        $this->addSql('DROP TABLE deleted_accounts');
        $this->addSql('DROP TABLE platforms');
        $this->addSql('DROP TABLE markers');
        $this->addSql('DROP TABLE problem_tickets');
        $this->addSql('DROP TABLE servers');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE games');
        $this->addSql('DROP TABLE game_versions');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE problem_tickets_community');
    }
}
