<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805064846 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('ALTER TABLE moderators ADD status_weight INT NOT NULL, CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE country_id country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user CHANGE salt salt VARCHAR(255) DEFAULT NULL, CHANGE last_login last_login DATETIME DEFAULT NULL, CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT NULL, CHANGE password_requested_at password_requested_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT NOT NULL, CHANGE store store INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT NOT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE game game INT NOT NULL, CHANGE server server INT NOT NULL, CHANGE platform platform INT NOT NULL, CHANGE version version INT NOT NULL, CHANGE tag tag INT NOT NULL, CHANGE marker marker INT NOT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT DEFAULT NULL');
        $this->addSql('UPDATE moderators SET status_weight = 1 WHERE status = "Действующий"');
        $this->addSql('UPDATE moderators SET status_weight = 2 WHERE status = "Ожидает"');
        $this->addSql('UPDATE moderators SET status_weight = 3 WHERE status = "Сам ушел"');
        $this->addSql('UPDATE moderators SET status_weight = 4 WHERE status = "Отклонен"');
        $this->addSql('UPDATE moderators SET status_weight = 5 WHERE status = "Изгнан"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE deleted_accounts CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user CHANGE salt salt VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE last_login last_login DATETIME DEFAULT \'NULL\', CHANGE confirmation_token confirmation_token VARCHAR(180) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE password_requested_at password_requested_at DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE game_versions CHANGE game game INT DEFAULT NULL');
        $this->addSql('ALTER TABLE markers CHANGE tag_id tag_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE moderators DROP status_weight, CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE country_id country_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE platform platform INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets_community CHANGE game game INT DEFAULT NULL, CHANGE server server INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem_tickets_store CHANGE game game INT DEFAULT NULL, CHANGE store store INT DEFAULT NULL, CHANGE version version INT DEFAULT NULL, CHANGE tag tag INT DEFAULT NULL, CHANGE marker marker INT DEFAULT NULL');
    }
}
