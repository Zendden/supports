  const SEARCH_TYPES = [
                'Filter',
                'tickets',
                'stores',
                'players',
            ];

            class ViewModel {

                constructor () {
                    self.data = {};
                    self.id = ko.observable ();
                    self.type = ko.observable ();
                    self.resultLink = ko.observable ();
                    self.resultLinkName = ko.observable ();
                    self.resultVisible = ko.observable (false);
                }

                async getDataResponse () {
                    let url = "/find-by/" + self.type () + "/" + self.id ();

                    const response = await fetch (url);
                    const dataResponse = await response.json ();

                    const dataJson = JSON.parse (dataResponse);
                    self.data = dataJson[0];

                    if (self.data.game) {
                        this.getLink (self.id ());
                        this.getLinkName ();
                        self.resultVisible (true);

                        console.log (self.resultVisible ());
                        console.log (self.resultLinkName ());
                    }
                }

                getLinkName () {
                    return resultLinkName (self.data.id + " " + self.data.game.title + " " + self.data.version.title);
                }
                

                getLink (linkId) {
                    return self.resultLink ("http://127.0.0.1:8000/show/" + linkId + "/problem-ticket");
                }
            }

            $(document).ready (function () {
                let vm = new ViewModel ();
                ko.applyBindings (vm, $("searchApp").get (0));
            });