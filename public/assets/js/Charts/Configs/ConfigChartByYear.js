let CONFIG = {
    chart: {
        type: 'column',
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: [],
        allowDecimals: false,
        title: {
            text: 'Period'
        },
    },
    yAxis: {
        title: {
            text: 'Tickets'
        }
    },
    tooltip: {
        valueSuffix: ' tickets',
    },
    plotOptions: {
        column: {
        },
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        self.formData.month = this.x + 1;
                        console.log (self.formData.month)

                            $.ajax ({
                            type: "post",
                            url: "/charts/get-by/game/",
                            dataType: "json",
                            data: self.formData,
                            success: response => {
                                CONFIG_CHILD.series = [];
                                for (let key in response["projects"]) {
                                    response["projects"][key].data.splice(0, 1);
                                    CONFIG_CHILD.series.push (response["projects"][key]);
                                }

                                for (var j = 0; j < CONFIG.xAxis.categories.length; j++) {
                                    if (j == self.formData.month - 1) {
                                        CONFIG_CHILD.title.text = CONFIG.xAxis.categories[j] + ": " + self.formData.year;
                                    }
                                }

                                CONFIG_CHILD.xAxis.categories = [];
                                for (var i = 1; i < response.charts.categories.length; i++) {
                                    CONFIG_CHILD.xAxis.categories.push (i);
                                }

                                Highcharts.chart('containerChild', CONFIG_CHILD);
                            },
                            error: response => {
                                console.log (response);
                            }
                        });
                    }
                }
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px"><span style="color:{point.color}">{series.name}</span></span><br>',
        pointFormat: 'Tickets: <b>{point.y}</b><br/>'
    },
    rangeSelector: {
        selected: 1
    },
    series: []
};