let CONFIG_PIE = {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Projects per current year'
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '<span style="font-size:14px">{point.name}: {point.y}</span>'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span> | Tickets: <b>{point.y}</b><br/>'
    },

    series: [
        {
        }
    ],
    drilldown: {
        series: [
            {
                data: []
            }
        ],
    }
}