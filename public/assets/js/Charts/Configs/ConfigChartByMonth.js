let CONFIG_CHILD = {
    chart: {
        type: 'column',
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: [],
        allowDecimals: false,
        title: {
            text: 'Period'
        },
    },
    yAxis: {
        title: {
            text: 'Tickets'
        }
    },
    plotOptions: {
        column: {
        },
        series: {
            cursor: 'cursor',
        }
    },
    tooltip: {
    headerFormat: '<span style="font-size:11px"><span style="color:{point.color}">{series.name}</span></span><br>',
    pointFormat: 'Tickets: <b>{point.y}</b>'
    },
    rangeSelector: {
        selected: 1
    },
    series: []
};