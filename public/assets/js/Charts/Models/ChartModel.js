class Charts {
    constructor () {
        self.chart = CONFIG;
        const formData = {};

        self.game = ko.observable ();
        self.version = ko.observable ();
        self.server = ko.observable ();
        self.platform = ko.observable ();
        self.store = ko.observable ();
        self.tag = ko.observable ();
        self.year = ko.observable ();

        self.choiseLocation = ko.observable ();
        self.choiseType = ko.observable ();
        self.switcher = ko.observable (false);
    }

    getCharts () {
        self.formData = {
            "location": self.choiseLocation (),
            "type": self.choiseType (),
            "year": self.year (),
            "month": null,
            "game": self.game (),
            "version": self.version (),
            "server": self.server (),
            "platform": self.platform (),
            "store": self.store (),
            "tag": self.tag ()
        };

        $.ajax ({
            type: "post",
            url: "/charts/get-by/game/",
            dataType: "json",
            data: self.formData,
            success: response => {
                CONFIG.series = [];
                CONFIG.title.text = "Year: " + self.formData.year;
                CONFIG.xAxis.categories = response.charts.categories;

                if (self.switcher ()) {
                    CONFIG.plotOptions.column = {};
                    CONFIG_CHILD.plotOptions.column = {};
                } else {
                    CONFIG.plotOptions.column.stacking = "percent";
                    CONFIG_CHILD.plotOptions.column.stacking = "percent";
                }
                
                for (let key in response["projects"]) {
                    CONFIG.series.push (response["projects"][key]);
                }

                Highcharts.chart('containerBase', CONFIG);
            }
        });
    }

    getPieChart () {
        $.ajax ({
            type: 'post',
            url: "/charts/get-for/pie",
            dataType: 'json',
            data: {'pie': 'pie'},
            success: response => {
                response.map (item => {
                    CONFIG_PIE.series = item.series; 
                    item.drilldown.series.forEach(g => {
                        g.data.forEach(v => {
                            var string = new String (v[0]);
                            v[0] = string.toString ();
                            
                        });
                    }); 
                    CONFIG_PIE.drilldown = item.drilldown;
                });

                Highcharts.chart('containerPie', CONFIG_PIE);
            }
        });  
        
    }
}

$(document).ready (function () {
    let charts = new Charts ();
    charts.getPieChart ();
    ko.applyBindings (charts, document.getElementById('chart'));
});
ko.cleanNode(document.getElementById('chart'));
